# FROM iron/java:1.8
FROM java:8-jre-alpine

# Prepare by downloading dependencies
COPY ./viomi-analysis-web/target/viomi-analysis-web-1.0-SNAPSHOT.jar /home/

VOLUME ["/home/logs"]
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Duser.timezone=GMT","-XX:+PrintGCDateStamps","-XX:+PrintGCTimeStamps","-XX:+PrintGCDetails","-XX:+HeapDumpOnOutOfMemoryError","-Xloggc:logs/gc_1.log","-jar","/home/viomi-analysis-web-1.0-SNAPSHOT.jar"]
#,"--spring.profiles.active=prod"
