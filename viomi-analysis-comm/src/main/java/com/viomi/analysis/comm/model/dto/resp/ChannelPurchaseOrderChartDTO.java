package com.viomi.analysis.comm.model.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author Shubifeng
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "渠道预付款统计汇总")
public class ChannelPurchaseOrderChartDTO implements Serializable {

    @ApiModelProperty(value = "汇总数据")
    private ChannelPurchaseOrderTotalDTO totalData;

    @ApiModelProperty(value = "列表数据")
    private List<ChannelPurchaseOrderResp> listData;
}
