package com.viomi.analysis.comm.model.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author Shubifeng
 */
@ApiModel(description = "渠道预付款订单统计列表")
@Data
@ToString
public class ChannelPurchaseOrderResp implements Serializable {

    @ApiModelProperty(value = "数据时间(年2017，月2017-08，日2017-01-01)", example = "2017-08-01")
    private String dataTime;

    @ApiModelProperty(value = "提货订单数", example = "81")
    private Long orderNum = 0L;

    @ApiModelProperty(value = "提货金额", example = "2281")
    private Long orderFee = 0L;

    @ApiModelProperty(value = "退货退款订单", example = "1")
    private Long refundOrderNum = 0L;

    @ApiModelProperty(value = "退货退款额", example = "61")
    private Long refundOrderFee = 0L;

}
