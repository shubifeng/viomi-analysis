package com.viomi.analysis.comm.util.export.genrator;

import com.viomi.analysis.comm.constant.POIConstants;
import io.swagger.annotations.ApiModelProperty;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Excel导出类
 * HSSFWorkbook：支持EXCEL2003(.xls);最多65535行，一般不会发生内存溢出（OOM）；
 * XSSFWorkbook：支持EXCEL2007++(.xlsx);最多1048576（104万）行，16384列)，容易引起OOM；
 * SXSSFWorkbook：SXSSFWorkbook(poi.jar 3.8+)可以设置最大内存量(new SXSSFWookbook(rowAccessWindowSize))，超过rowAccessWindowSize则持久化到文件，从而避免OOM；
 */
public abstract class ExportGenerator implements IExportGenerator {

    /**
     * 金额保留两位小数
     */
    public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.00");
    /**
     * 分批处理的list大小
     */
    private static final int SUB_LIST_SIZE = 10000;

    /**
     * 分组List数据
     *
     * @param orgList
     * @param subSize
     * @param <T>
     * @return
     */
    public static <T> List<Map<String, List<T>>> createSubLists( List<T> orgList, int subSize ) {
        if (null == orgList || 0 == subSize) {
            return null;
        }

        List<Map<String, List<T>>> subLists = new ArrayList<Map<String, List<T>>>();
        int listNum = orgList.size() / subSize;
        int lastSubsNum = orgList.size() % subSize;

        for (int i = 0; i < listNum; i++) {
            Map<String, List<T>> subMap = new HashMap<>();
            List<T> subList = orgList.subList(i * subSize, (i + 1) * subSize);
            //数据范围为name
            subMap.put(String.format("%s-%s", i * subSize, (i + 1) * subSize), subList);

            subLists.add(subMap);
        }
        if (0 != lastSubsNum) {
            Map<String, List<T>> subMap = new HashMap<>();
            List<T> subList = orgList.subList(listNum * subSize, listNum * subSize + lastSubsNum);
            subMap.put(String.format("%s-%s", listNum * subSize, listNum * subSize + lastSubsNum), subList);
            subLists.add(subMap);
        }
        return subLists;
    }

    /**
     * 创建Workbook
     *
     * @param <T>
     * @param listData
     * @return
     */
    public <T> Workbook buildWorkbook( List<T> listData ) {
        int rowNum = 0;
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet s = wb.createSheet(listData.size() + "");
        XSSFRow r = null;
        POIConstants pct = POIConstants.POIConstantsInstanceFactory.initWithWorkbook(wb);

        r = s.getRow(rowNum);
        if (r == null) {
            r = s.createRow(rowNum);
        }
        buildHeadRow(r, pct);

        for (Object val : listData) {
            r = s.getRow(++rowNum);
            if (r == null) {
                r = s.createRow(rowNum);
            }
            //创建行数据，需要在具体业务类中实现具体逻辑
            buildRow(val, r, pct);
        }

        return wb;
    }

    /**
     * 创建Workbook,按指定数据量分多个sheet
     *
     * @param <T>
     * @param listData
     * @return
     */
    public <T> Workbook buildWorkbookForSubSheet( List<T> listData ) {
        XSSFWorkbook wb = new XSSFWorkbook();
        POIConstants pct = POIConstants.POIConstantsInstanceFactory.initWithWorkbook(wb);

        //分组生成sheet
        List<Map<String, List<T>>> subLists = createSubLists(listData, SUB_LIST_SIZE);
        subLists.forEach(subMap -> buildWorkbookBySheetName(wb, subMap, pct));

        return wb;
    }

    private <T> void buildWorkbookBySheetName( XSSFWorkbook wb, Map<String, List<T>> subMap, POIConstants pct ) {
        String sname = String.valueOf(subMap.keySet().toArray()[0]);
        List subList = subMap.get(sname);

        int subRowNum = 0;
        XSSFSheet subSheet = wb.createSheet(sname);

        XSSFRow headRow = subSheet.getRow(subRowNum);
        if (headRow == null) {
            headRow = subSheet.createRow(subRowNum);
        }
        buildHeadRow(headRow, pct);

        XSSFRow r;
        for (Object val : subList) {
            r = subSheet.getRow(++subRowNum);
            if (r == null) {
                r = subSheet.createRow(subRowNum);
            }
            //创建行数据，需要在具体业务类中实现具体逻辑
            buildRow(val, r, pct);
        }
    }

    /**
     * 创建表头
     *
     * @param r
     * @param pct
     */
    public void buildHeadRow( Row r, POIConstants pct ) {
        Class retClass = getEntityClass();
        /**
         * retClass.getDeclaredFields()只能获取子类的属性；getFields()只能获取子类的公有属性，包括父类的公有属性。
         * 递归查询父类属性
         */
        List<Field> fieldList = new ArrayList<>();
        //当父类为null的时候说明到达了最上层的父类(Object类).
        while (retClass != null && !retClass.equals("java.lang.object")) {
            fieldList.addAll(Arrays.asList(retClass.getDeclaredFields()));
            //得到父类,然后赋给自己
            retClass = retClass.getSuperclass();
        }

        /*根据position排序*/
        fieldList = fieldList.stream().sorted(( f1, f2 ) -> {
            Integer fi1 = Integer.valueOf(String.valueOf(AnnotationUtils.getValue(f1.getAnnotationsByType(ApiModelProperty.class)[0], "position")));
            Integer fi2 = Integer.valueOf(String.valueOf(AnnotationUtils.getValue(f2.getAnnotationsByType(ApiModelProperty.class)[0], "position")));
            return fi1.compareTo(fi2);
        }).collect(Collectors.toList());

        int cellNum = 0;
        for (Field f : fieldList) {
            //过滤：只处理含有@ApiModelProperty的字段
            if (f.getAnnotationsByType(ApiModelProperty.class).length != 0 && f.getAnnotations().length == 1) {
                String fieldName = String.valueOf(AnnotationUtils.getValue(f.getAnnotationsByType(ApiModelProperty.class)[0], "value"));
                Cell cell = r.createCell(cellNum++);
                cell.setCellStyle(pct.getHeadStyle());
                cell.setCellValue(fieldName == null ? " - " : fieldName);
            }
        }
    }
}
