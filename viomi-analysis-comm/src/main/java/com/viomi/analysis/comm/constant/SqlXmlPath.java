package com.viomi.analysis.comm.constant;

/**
 * Created by asus on 2018/2/1.
 */
public interface SqlXmlPath {

    String WARES_CATALOG = "/sql/wares_catalog.xml";
    String WARES = "/sql/wares.xml";
    String CHANNEL_PURCHASE = "/sql/channel_purchase.xml";
    String USER = "/sql/user.xml";
    String ORDER_DELIVERY = "/sql/order_delivery.xml";
    String ORDER_REFUND = "/sql/order_refund.xml";

}
