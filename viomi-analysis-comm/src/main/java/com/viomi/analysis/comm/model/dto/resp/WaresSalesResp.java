package com.viomi.analysis.comm.model.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author Shubifeng
 */
@Data
@ToString
@ApiModel(description = "某个时间内商品的销售量与销售额")
public class WaresSalesResp implements Serializable {

    @ApiModelProperty(value = "x轴数据时间(年2017，月2017-08，日2017-01-01)", example = "2017-08-01")
    private String data_time;

    @ApiModelProperty(value = "y轴商品销售量", example = "1929")
    private String quantity;

    @ApiModelProperty(value = "y轴商品销售额", example = "2000000")
    private String sales;
}
