package com.viomi.analysis.comm.model.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author Shubifeng
 */
@ApiModel(description = "米家发货报表")
@Data
@ToString
public class OrderDeliveryMijiaResp extends OrderDeliveryResp implements Serializable {

    @ApiModelProperty(value = "米家订单", example = "123456", position = 21)
    private String mijiaOrderCode;

}
