package com.viomi.analysis.comm.util.export;

import com.viomi.analysis.comm.constant.POIConstants;
import com.viomi.analysis.comm.model.dto.resp.OrderDeliveryPurchaseResp;
import com.viomi.analysis.comm.util.export.genrator.ExportGenerator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellUtil;

import java.util.Optional;

/**
 * 预付款 发货报表 导出实现类
 *
 * @Author: luocj
 * @Date: Created in 2018/4/27 18:16
 * @Modified:
 */
public class OrderDeliveryPurchaseGenerator extends ExportGenerator {
    @Override
    public Class getEntityClass() {
        return OrderDeliveryPurchaseResp.class;
    }

    @Override
    public void buildRow( Object obj, Row r, POIConstants pct ) {
        OrderDeliveryPurchaseResp val = (OrderDeliveryPurchaseResp) obj;

        int cellNum = 0;

        /*严格按照配置position顺序*/
        //云米订单
        CellUtil.createCell(r, cellNum++, val.getOrderCode(), null);
        //商品名称
        CellUtil.createCell(r, cellNum++, val.getSkuName(), null);
        //商品69码
        CellUtil.createCell(r, cellNum++, val.getBarCode(), null);
        //数量
        CellUtil.createCell(r, cellNum++, String.valueOf(val.getQuantity()), null);
        //零售价
        Double retailPrice = Optional.ofNullable(val.getRetailPrice()).orElse(0D);
        CellUtil.createCell(r, cellNum++, String.valueOf(DECIMAL_FORMAT.format((double) retailPrice / 100)), pct.getFloatStyle2());
        //金额
        Double price = Optional.ofNullable(val.getPrice()).orElse(0D);
        CellUtil.createCell(r, cellNum++, String.valueOf(DECIMAL_FORMAT.format((double) price / 100)), pct.getFloatStyle2());
        //订单时间
        CellUtil.createCell(r, cellNum++, val.getCreatedTime(), null);
        //发货时间
        CellUtil.createCell(r, cellNum++, val.getSentTime(), null);
        //提货单价
        Long standardPrice = Optional.ofNullable(val.getStandardPrice()).orElse(0L);
        CellUtil.createCell(r, cellNum++, String.valueOf(DECIMAL_FORMAT.format((double) standardPrice / 100)), pct.getFloatStyle2());
        //提成抵扣金额
        Long profitPrice = Optional.ofNullable(val.getProfitPrice()).orElse(0L);
        CellUtil.createCell(r, cellNum++, String.valueOf(DECIMAL_FORMAT.format((double) profitPrice / 100)), pct.getFloatStyle2());
        //运费
        Long deliveryFee = Optional.ofNullable(val.getDeliveryFee()).orElse(0L);
        CellUtil.createCell(r, cellNum++, String.valueOf(DECIMAL_FORMAT.format((double) deliveryFee / 100)), pct.getFloatStyle2());
        //产品实际支付价
        Long actualPrice = Optional.ofNullable(val.getActualPrice()).orElse(0L);
        CellUtil.createCell(r, cellNum++, String.valueOf(DECIMAL_FORMAT.format((double) actualPrice / 100)), pct.getFloatStyle2());
        //渠道编码
        CellUtil.createCell(r, cellNum++, val.getChannelCode(), null);
        //渠道名称
        CellUtil.createCell(r, cellNum++, val.getChannelName(), null);
        //所属大区
        CellUtil.createCell(r, cellNum++, val.getSuperOrgName(), null);
        //所属分部
        CellUtil.createCell(r, cellNum++, val.getOrgName(), null);
        //订单平台
        CellUtil.createCell(r, cellNum++, val.getPlatformType(), null);
        //平台订单号
        CellUtil.createCell(r, cellNum++, val.getPlatformOrderNum(), null);
        //原始订单
        CellUtil.createCell(r, cellNum++, val.getOriginalCode(), null);
        //发货仓
        CellUtil.createCell(r, cellNum++, val.getExpressWarehouseName(), null);
        //物流公司
        CellUtil.createCell(r, cellNum++, val.getExpressCorpName(), null);
        //物流单号
        CellUtil.createCell(r, cellNum++, val.getExpressNo(), null);
        //开票状态
        CellUtil.createCell(r, cellNum++, val.getInvoiceStatus(), null);
        //是否物料
        CellUtil.createCell(r, cellNum++, val.getOrderType(), null);
        //订单备注
        CellUtil.createCell(r, cellNum++, val.getOrderRemark(), null);
    }
}
