package com.viomi.analysis.comm.model.bo;

import com.viomi.base.common.constant.Req;
import com.viomi.base.model.dto.req.PageReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 预付款订单查询条件
 * <p>
 * refer to vstore PurchaseOrderQueryReq.java
 */
@Data
@NoArgsConstructor
public class PurchaseOrderQueryBO extends PageReq implements Serializable {


    @ApiModelProperty(value = "渠道名称")
    private String channelName;

    @ApiModelProperty(value = "收货人姓名")
    private String receiver;

    @ApiModelProperty(value = "收货人手机")
    private String receiverMobile;

    @ApiModelProperty(value = "商品skuID")
    private List<Long> skuIds;

    @ApiModelProperty(value = "商品名称")
    private String productName;

    @ApiModelProperty(value = Req.purchaseOrderDesc)
    private Integer orderType;

    @ApiModelProperty(value = "订单状态")
    private List<Integer> orderStatus;

    @ApiModelProperty(value = "开票状态")
    private Integer invoiceFlag;

    @ApiModelProperty(value = "平台类型")
    private Integer platformType;

    @ApiModelProperty(value = "平台订单编号")
    private String platformOrderNum;

    @ApiModelProperty(value = "订单编码")
    private String orderCode;

    @ApiModelProperty(value = "开始时间")
    private Long beginTime;

    @ApiModelProperty(value = "结束时间")
    private Long endTime;

    @ApiModelProperty(value = "快递单号")
    private String expressNo;

    @ApiModelProperty(value = "收货地址，地区编码: 到区级的编码")
    private String division;

    @ApiModelProperty(hidden = true)
    private Long orgId;

    @ApiModelProperty(hidden = true)
    private Boolean safe;

    @ApiModelProperty(hidden = true)
    private Long channelId;
}
