package com.viomi.analysis.comm.util.export;

import com.viomi.analysis.comm.constant.POIConstants;
import com.viomi.analysis.comm.model.dto.resp.OrderDeliveryViomiResp;
import com.viomi.analysis.comm.util.export.genrator.ExportGenerator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellUtil;

import java.util.Optional;

/**
 * 云米商城 发货报表 导出实现类
 *
 * @Author: luocj
 * @Date: Created in 2018/4/27 18:16
 * @Modified:
 */
public class OrderDeliveryViomiGenerator extends ExportGenerator {
    @Override
    public Class getEntityClass() {
        return OrderDeliveryViomiResp.class;
    }

    @Override
    public void buildRow( Object obj, Row r, POIConstants pct ) {
        OrderDeliveryViomiResp val = (OrderDeliveryViomiResp) obj;

        int cellNum = 0;

        /*严格按照配置position顺序*/
        //云米订单
        CellUtil.createCell(r, cellNum++, val.getOrderCode(), null);
        //商品名称
        CellUtil.createCell(r, cellNum++, val.getSkuName(), null);
        //商品69码
        CellUtil.createCell(r, cellNum++, val.getBarCode(), null);
        //数量
        CellUtil.createCell(r, cellNum++, String.valueOf(val.getQuantity()), null);
        //零售价
        Double retailPrice = Optional.ofNullable(val.getRetailPrice()).orElse(0D);
        CellUtil.createCell(r, cellNum++, String.valueOf(DECIMAL_FORMAT.format((double) retailPrice)), pct.getFloatStyle2());
        //金额
        Double price = Optional.ofNullable(val.getPrice()).orElse(0D);
        CellUtil.createCell(r, cellNum++, String.valueOf(DECIMAL_FORMAT.format((double) price)), pct.getFloatStyle2());
        //订单时间
        CellUtil.createCell(r, cellNum++, val.getCreatedTime(), null);
        //发货时间
        CellUtil.createCell(r, cellNum++, val.getSentTime(), null);
        //是否F2C
        CellUtil.createCell(r, cellNum++, val.getIsF2c(), null);
        //利润分成对象
        CellUtil.createCell(r, cellNum++, val.getProfitShareDesc(), null);
        //销售门店
        CellUtil.createCell(r, cellNum++, val.getTerminalName(), null);
        //一级经销商
        CellUtil.createCell(r, cellNum++, val.getCityAgentName(), null);
        //城市运营商编码
        CellUtil.createCell(r, cellNum++, val.getCityAgentCode(), null);
        //发货仓
        CellUtil.createCell(r, cellNum++, val.getExpressWarehouseName(), null);
        //物流公司
        CellUtil.createCell(r, cellNum++, val.getExpressCorpName(), null);
        //物流单号
        CellUtil.createCell(r, cellNum++, val.getExpressNo(), null);
        //开票状态
        CellUtil.createCell(r, cellNum++, val.getInvoiceStatus(), null);
    }
}
