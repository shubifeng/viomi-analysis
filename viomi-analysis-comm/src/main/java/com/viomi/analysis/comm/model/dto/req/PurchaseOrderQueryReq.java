package com.viomi.analysis.comm.model.dto.req;

import com.viomi.base.common.constant.Req;
import com.viomi.base.model.dto.req.PageReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
/**
 * 预付款订单查询条件
 *
 * refer to vstore PurchaseOrderQueryReq.java
 *
 */
@Data
@NoArgsConstructor
public class PurchaseOrderQueryReq extends PageReq implements Serializable
{


    @ApiModelProperty(value = "开始时间")
    private Long beginTime;

    @ApiModelProperty(value = "结束时间")
    private Long endTime;

    @ApiModelProperty(value = "平台订单编号")
    private String platformOrderNum;

    @ApiModelProperty(value = "订单状态")
    private List<Integer> orderStatus;

    @ApiModelProperty(value = "商品名称")
    private String productName;

    @ApiModelProperty(value = "商家名称，仅有非云米平台时有效")
    private String channelName;

    @ApiModelProperty(value = Req.purchaseOrderDesc)
    private Integer orderType;


}
