package com.viomi.analysis.comm.util.export;

import com.viomi.analysis.comm.constant.POIConstants;
import com.viomi.analysis.comm.model.dto.resp.OrderDeliveryMijiaResp;
import com.viomi.analysis.comm.util.export.genrator.ExportGenerator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellUtil;

import java.util.Optional;

/**
 * 米家 发货报表 导出实现类
 *
 * @Author: luocj
 * @Date: Created in 2018/4/27 18:16
 * @Modified:
 */
public class OrderDeliveryMijiaGenerator extends ExportGenerator {
    @Override
    public Class getEntityClass() {
        return OrderDeliveryMijiaResp.class;
    }

    @Override
    public void buildRow( Object obj, Row r, POIConstants pct ) {
        OrderDeliveryMijiaResp val = (OrderDeliveryMijiaResp) obj;

        int cellNum = 0;

        /*严格按照配置position顺序*/
        //云米订单
        CellUtil.createCell(r, cellNum++, val.getOrderCode(), null);
        //商品名称
        CellUtil.createCell(r, cellNum++, val.getSkuName(), null);
        //商品69码
        CellUtil.createCell(r, cellNum++, val.getBarCode(), null);
        //数量
        CellUtil.createCell(r, cellNum++, String.valueOf(val.getQuantity()), null);
        //零售价
        Double retailPrice = Optional.ofNullable(val.getRetailPrice()).orElse(0D);
        CellUtil.createCell(r, cellNum++, String.valueOf(DECIMAL_FORMAT.format((double) retailPrice / 100)), pct.getFloatStyle2());
        //金额
        Double price = Optional.ofNullable(val.getPrice()).orElse(0D);
        CellUtil.createCell(r, cellNum++, String.valueOf(DECIMAL_FORMAT.format((double) price / 100)), pct.getFloatStyle2());
        //订单时间
        CellUtil.createCell(r, cellNum++, val.getCreatedTime(), null);
        //发货时间
        CellUtil.createCell(r, cellNum++, val.getSentTime(), null);
        //米家订单
        CellUtil.createCell(r, cellNum++, val.getMijiaOrderCode(), null);
        //发货仓
        CellUtil.createCell(r, cellNum++, val.getExpressWarehouseName(), null);
        //物流公司
        CellUtil.createCell(r, cellNum++, val.getExpressCorpName(), null);
        //物流单号
        CellUtil.createCell(r, cellNum++, val.getExpressNo(), null);
        //开票状态
        CellUtil.createCell(r, cellNum++, val.getInvoiceStatus(), null);
    }
}
