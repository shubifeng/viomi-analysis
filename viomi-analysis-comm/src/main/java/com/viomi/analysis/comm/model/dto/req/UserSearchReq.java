package com.viomi.analysis.comm.model.dto.req;

import com.viomi.analysis.comm.constant.SourceType;
import com.viomi.base.model.dto.req.PageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by asus on 2018/3/29.
 */
@Data
@ApiModel(description = "用户搜索")
public class UserSearchReq extends PageReq implements Serializable {

    @ApiModelProperty(value = "用户名")
    private String userName;
    @ApiModelProperty(value = "联系电话,值为@时表示查询不存在手机号码的用户记录")
    private String phone;
    @ApiModelProperty(value = "云米账号,值为@时表示查询不存在云米账号的用户记录")
    private String viomiNo;
    @ApiModelProperty(value = "注册时间,格式:2017-07-07 12:00:00|2017-09-09 12:30:30,值为@时表示查询非注册的用户记录")
    private String queryTime;
    @ApiModelProperty(value = "用户来源")
    private SourceType sourceType;

}



