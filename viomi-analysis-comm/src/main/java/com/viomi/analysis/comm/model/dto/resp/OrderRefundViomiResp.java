package com.viomi.analysis.comm.model.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * @Author: luocj
 * @Date: Created in 2018/7/16 15:17
 * @Modified:
 */
@ApiModel(description = "云米商城 退货退款报表")
@Data
@ToString
public class OrderRefundViomiResp extends OrderRefundResp {

    @ApiModelProperty(value = "工单号", example = "", position = 1)
    private String serviceCode;
    @ApiModelProperty(value = "退回仓库", example = "", position = 31)
    private String refundToWarehouse;
    @ApiModelProperty(value = "退款平台", example = "", position = 32)
    private String refundPlatform;
}
