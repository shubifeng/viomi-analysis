package com.viomi.analysis.comm.constant.report;

/**
 *
 */
public enum PurchaseOrderAttrCode
{
    //
    INVOICE_TYPE("invoice_type", "发票类型"),
    //
    INVOICE_OBJECT("invoice_object", "开票对象"),
    //
    INVOICE_TITLE("invoice_title", "发票抬头"),
    //
    TAXPAYER_CODE("taxpayer_code", "纳税人识别号"),
    //
    EXPRESS_TYPE("express_type", "快递类型"),
    //
    EXPRESS_NUMBER("express_number", "快递编号"),
    //
    INVOICE_FLAG("invoice_flag", "开票标志"),
    //
    PLATFORM_TYPE("platform_type", "平台类型:1-天猫;2-京东;3-苏宁;4-其他"),
    //
    PLATFORM_ORDER_NUM("platform_order_num", "平台订单编号"),
    
    PLATFORM_ORDER_PRICE("platform_order_price", "平台订单价格"),
    //
    ORDER_REMARK("order_remark", "订单备注"),
    //
    ORIGINAL_ORDER("original_order", "订单拆分之前的原始订单编号"),
    //
    SENT_WAREHOUSE_NO("warehouse_no", "发货仓编号"),
    SENT_WAREHOUSE("warehouse", "发货仓"),
    
    SENT_WAREHOUSE_COMPANY("warehouse_company", "发货仓公司");
    
    private String code;
    
    private String desc;
    
    private PurchaseOrderAttrCode(String code, String desc)
    {
        this.code = code;
        this.desc = desc;
    }
    
    public String getCode()
    {
        return code;
    }
    
    public String getDesc()
    {
        return desc;
    }
}
