package com.viomi.analysis.comm.model.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by asus on 2018/4/2.
 */
@Data
@ApiModel(description = "融合用户")
public class UniqueUserResp implements Serializable {

    @ApiModelProperty(value = "云米账号", example = "20035828")
    private String vmNo;

    @ApiModelProperty(value = "用户来源类型", example = "shop-app")
    private String sourceType;

    @ApiModelProperty(value = "来源数据id", example = "37723")
    private String dataId;

    @ApiModelProperty(value = "姓名", example = "张三丰")
    private String userName;

    @ApiModelProperty(value = "微信昵称", example = "张三")
    private String wxNickname;

    @ApiModelProperty(value = "微信图像", example = "http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI1He")
    private String wxHeadImg;

    @ApiModelProperty(value = "联系电话", example = "18820003000")
    private String phone;

    @ApiModelProperty(value = "首次发生时间", example = "2018-02-10 23:33:33")
    private String createdTime;

    @ApiModelProperty(value = "注册时间", example = "2018-02-10 23:33:33")
    private String regTime;
}
