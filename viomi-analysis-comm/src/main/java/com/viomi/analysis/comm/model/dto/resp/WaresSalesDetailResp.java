package com.viomi.analysis.comm.model.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * @author Shubifeng
 */
@Slf4j
@ToString
@Data
@ApiModel(description = "某个时间内某一种商品的销售量与销售额")
public class WaresSalesDetailResp implements Serializable {

    @ApiModelProperty(value = "商品名称", example = "净水器x2")
    private String name;

    @ApiModelProperty(value = "商品销量", example = "2003")
    private Integer quantity;

    @ApiModelProperty(value = "商品销额", example = "23323344")
    private Long sales;
}
