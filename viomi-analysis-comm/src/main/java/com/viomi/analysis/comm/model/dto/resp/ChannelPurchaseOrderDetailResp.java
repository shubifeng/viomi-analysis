package com.viomi.analysis.comm.model.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author Shubifeng
 */
@ApiModel(description = "渠道预付款订单统计详情")
@Data
@ToString
public class ChannelPurchaseOrderDetailResp implements Serializable {

    @ApiModelProperty(value = "渠道ID")
    private Long channelId;

    @ApiModelProperty(value = "门店名称", position = 1)
    private String terminalName;

    @ApiModelProperty(value = "经销商名称", position = 2)
    private String regionalAgentName;

    @ApiModelProperty(value = "城市运营名称", position = 3)
    private String cityAgentName;

    @ApiModelProperty(value = "提货订单数")
    private Long orderNum = 0L;

    @ApiModelProperty(value = "提货金额")
    private Long orderFee = 0L;

    @ApiModelProperty(value = "商品数")
    private Long productNum = 0L;

}
