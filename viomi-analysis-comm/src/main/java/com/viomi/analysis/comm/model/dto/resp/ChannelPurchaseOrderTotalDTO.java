package com.viomi.analysis.comm.model.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Shubifeng
 */
@Data
@NoArgsConstructor
@ApiModel(description = "数据汇总")
public class ChannelPurchaseOrderTotalDTO implements Serializable {

    @ApiModelProperty(value = "提货订单数", example = "10")
    private Long orderNum = 0L;

    @ApiModelProperty(value = "提货金额", example = "110")
    private Long orderFee = 0L;

    @ApiModelProperty(value = "退货退款订单", example = "2")
    private Long refundOrderNum = 0L;

    @ApiModelProperty(value = "退货退款额", example = "30")
    private Long refundOrderFee = 0L;
}
