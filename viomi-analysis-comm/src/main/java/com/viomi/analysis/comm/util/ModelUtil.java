package com.viomi.analysis.comm.util;

import com.viomi.boot.myjdbc.core.MyJdbcNamedModel;
import com.viomi.boot.myjdbc.core.MyJdbcPNamedModel;

import java.util.HashMap;
import java.util.Map;

/**
 * MyJdbcPNamedModel，MyJdbcPNamedModel工具类
 *
 * @Author: luocj
 * @Date: Created in 2018/7/16 15:56
 * @Modified:
 */
public class ModelUtil {

    public static MyJdbcPNamedModel getMyJdbcPNamedModel( String SQL, Long beginTime, Long endTime, Integer pageNum, Integer pageSize ) {
        Map<String, Object> map = new HashMap<>();
        map.put("v_btimestamp", beginTime);
        map.put("v_etimestamp", endTime);

        MyJdbcPNamedModel myJdbcPNamedModel = new MyJdbcPNamedModel();
        myJdbcPNamedModel.setMapParams(map);
        String sql = String.format(SQL, beginTime, endTime);
        myJdbcPNamedModel.setSql(sql);

        //分页信息
        myJdbcPNamedModel.setPageNo(pageNum);
        myJdbcPNamedModel.setPageSize(pageSize);
        return myJdbcPNamedModel;
    }

    public static MyJdbcNamedModel getMyJdbcNamedModel( String SQL, Long beginTime, Long endTime ) {
        Map<String, Object> map = new HashMap<>();
        map.put("v_btimestamp", beginTime);
        map.put("v_etimestamp", endTime);

        MyJdbcNamedModel myJdbcNamedModel = new MyJdbcNamedModel();
        myJdbcNamedModel.setMapParams(map);
        String sql = String.format(SQL, beginTime, endTime);
        myJdbcNamedModel.setSql(sql);

        return myJdbcNamedModel;
    }
}
