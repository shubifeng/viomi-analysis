package com.viomi.analysis.comm.model.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author Shubifeng
 */
@ApiModel(description = "云米商城发货报表")
@Data
@ToString
public class OrderDeliveryViomiResp extends OrderDeliveryResp implements Serializable {

    @ApiModelProperty(value = "是否F2C", example = "是", position = 21)
    private String isF2c;
    @ApiModelProperty(value = "利润分成对象", example = "个人和渠道商", position = 22)
    private String profitShareDesc;
    @ApiModelProperty(value = "销售门店", example = "销售门店", position = 23)
    private String terminalName;
    @ApiModelProperty(value = "一级经销商", example = "一级经销商", position = 24)
    private String cityAgentName;
    @ApiModelProperty(value = "城市运营商编码", example = "城市运营商编码", position = 25)
    private String cityAgentCode;

}
