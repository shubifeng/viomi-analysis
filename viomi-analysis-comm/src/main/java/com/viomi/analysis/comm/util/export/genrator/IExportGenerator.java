package com.viomi.analysis.comm.util.export.genrator;

import com.viomi.analysis.comm.constant.POIConstants;
import org.apache.poi.ss.usermodel.Row;

/**
 * Excel导出接口类
 *
 * @Author: luocj
 * @Date: Created in 2018/4/27 18:20
 * @Modified:
 */
public interface IExportGenerator {

    /**
     * 在具体业务类中实现，返回业务实体class
     * @return
     */
    public Class getEntityClass();

    /**
     * 创建行数据，需要在具体业务类中实现具体逻辑，例:
     *    TestClass val = (TestClass) obj;
     *    int cellNum = 0;
     *    //按次序创建文件（普通格式设置pct为null）
     *    createCell(r, cellNum++, val.getTestValue(), pct.getFloatStyle2());
     *
     * @param obj
     * @param r
     * @param pct
     * @param <T>
     */
    public <T> void buildRow( T obj, Row r, POIConstants pct );
}
