package com.viomi.analysis.comm.constant.report;

/**
 * 退货退款报表常量
 */

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RefundType {
    //    MIJIA(1, "米家-退货退款报表", "order_refund_mijia"),
    VIOMI(2, "云米商城-退货退款报表", "order_refund_viomi"),
    PURCHASE(3, "预付款-退货退款报表", "order_refund_purchase");

    private Integer type;
    private String desc;
    private String sqlKey;

    public static String getDesc( Integer type ) {
        for (RefundType r : RefundType.values()) {
            if (r.getType().equals(type)) {
                return r.getDesc();
            }
        }
        return "未知";
    }

}
