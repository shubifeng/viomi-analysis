package com.viomi.analysis.comm.model.dto.resp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author Shubifeng
 */
@ApiModel(description = "发货报表")
@Data
@ToString
public class OrderDeliveryResp implements Serializable {

    @ApiModelProperty(value = "商品ID", example = "1")
    @JsonIgnore
    private String skuId;
    @ApiModelProperty(value = "物流ID", example = "1")
    @JsonIgnore
    private String expressId;
    @ApiModelProperty(value = "订单ID", example = "1")
    @JsonIgnore
    private String orderId;
    @ApiModelProperty(value = "云米订单", example = "654321", position = 1)
    private String orderCode;
    @ApiModelProperty(value = "商品名称", example = "商品名称", position = 2)
    private String skuName;
    @ApiModelProperty(value = "商品69码", example = "111111", position = 3)
    private String barCode;
    @ApiModelProperty(value = "数量", example = "1", position = 4)
    private Long quantity = 0L;
    @ApiModelProperty(value = "零售价", example = "100", position = 5)
    private Double retailPrice = 0D;
    @ApiModelProperty(value = "金额", example = "100", position = 6)
    private Double price = 0D;
    @ApiModelProperty(value = "订单时间", example = "2018-01-01", position = 7)
    private String createdTime;
    @ApiModelProperty(value = "发货时间", example = "2018-01-01", position = 8)
    private String sentTime;

    @ApiModelProperty(value = "发货仓", example = "发货仓", position = 51)
    private String expressWarehouseName;
    @ApiModelProperty(value = "物流公司", example = "物流公司", position = 52)
    private String expressCorpName;
    @ApiModelProperty(value = "物流单号", example = "物流单号", position = 53)
    private String expressNo;
    @ApiModelProperty(value = "开票状态", example = "0", position = 54)
    private String invoiceStatus;

}
