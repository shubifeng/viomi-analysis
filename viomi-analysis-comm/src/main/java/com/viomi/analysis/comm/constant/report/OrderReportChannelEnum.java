package com.viomi.analysis.comm.constant.report;


// 报表-订单来源
public enum OrderReportChannelEnum {
    UNKNOWN(-100, "无法识别"),
    WEIXIN(1, "微信商城"),
    APP(2, "APP商城"),
    MIJIA(3, "米家"),
    YUNFENXIAO(4, "云分销"),
    TMALL(5, "天猫"),
    OTHER(99, "其他"),
    HEJI(100, "合计");

    private Integer channelId;

    private String name;

    private OrderReportChannelEnum(Integer channelId, String name) {
        this.channelId = channelId;
        this.name = name;
    }

    public static OrderReportChannelEnum getByCode(Integer channelId) {
        OrderReportChannelEnum s = UNKNOWN;
        if (channelId != null) {
            for (OrderReportChannelEnum e : OrderReportChannelEnum.values()) {
                if (e.channelId.equals(channelId)) {
                    s = e;
                    break;
                }
            }
        }
        return s;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public String getName() {
        return name;
    }

}
