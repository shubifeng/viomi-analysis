package com.viomi.analysis.comm.constant.report;

/**
 * 通用常量类
 *
 * @Author: luocj
 * @Date: Created in 2018/2/5 20:29
 * @Modified:
 */
public class CommonConstants {
    public static final Long SUPER_CHANNEL_ID = 1L;

    public static final Integer YES_OR_NO_NO=0;
    public static final Integer YES_OR_NO_YES=1;

    //refer to vstore CommonDivision.Version:1-国家统计局版2016
    public static final Byte DIVISION_VERSION = 1;
    public static final Integer DIVISION_LEVEL_PROVINCE=1;
    public static final Integer DIVISION_LEVEL_CITY=2;
    public static final Integer DIVISION_LEVEL_DISTRICT=3;
}
