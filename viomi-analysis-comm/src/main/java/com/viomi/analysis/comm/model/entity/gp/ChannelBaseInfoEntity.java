package com.viomi.analysis.comm.model.entity.gp;

import lombok.Data;

import java.io.Serializable;

/**
 * TODO
 *
 * @Author: luocj
 * @Date: Created in 2018/2/21 17:31
 * @Modified:
 */
@Data
public class ChannelBaseInfoEntity implements Serializable {
    // 根渠道编码
    public static final Long ROOT_CHANNEL_ID = 1L;

    // 云米虚拟渠道
    public static final Long VIRTUAL_CHANNEL_VIOMI = 214L;

    /**
     * channel_id
     */
    private Long id;

    /** 上级channel_id*/
    private Long parentId;

    // 生成逻辑:divisionCode+上一级渠道id+4位数的随机数
    private String code;

    // 名字唯一,不可重复
    /** 渠道名称 */
    private String name;

    private String divisionCode;

    private String address;

    private String phone;

    /** 渠道类型：1-城市运营/经销商;2-终端门店;3-第三方平台;*/
    private Byte type;

    private Byte status;

    private Byte approveStatus;


    private Long orgId;
}
