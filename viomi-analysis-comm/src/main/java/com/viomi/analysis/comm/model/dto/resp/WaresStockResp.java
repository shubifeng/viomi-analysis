package com.viomi.analysis.comm.model.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author Shubifeng
 */
@Data
@ToString
@ApiModel(description = "商品的存库与待发货数")
public class WaresStockResp implements Serializable {

    @ApiModelProperty(value = "商品名称", example = "免死卷X1")
    private String name;

    @ApiModelProperty(value = "商品类型名称", example = "虚拟")
    private String type_name;

    @ApiModelProperty(value = "代发货数", example = "100")
    private Integer pending_count;

    @ApiModelProperty(value = "库存数", example = "10")
    private Integer stock;
}
