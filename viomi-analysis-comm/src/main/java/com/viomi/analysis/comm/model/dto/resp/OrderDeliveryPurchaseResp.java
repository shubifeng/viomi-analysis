package com.viomi.analysis.comm.model.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author Shubifeng
 */
@ApiModel(description = "预付款发货报表")
@Data
@ToString
public class OrderDeliveryPurchaseResp extends OrderDeliveryResp implements Serializable {

    @ApiModelProperty(value = "提货单价", example = "100", position = 21)
    private Long standardPrice = 0L;
    @ApiModelProperty(value = "提成抵扣金额", example = "100", position = 22)
    private Long profitPrice = 0L;
    @ApiModelProperty(value = "运费", example = "100", position = 23)
    private Long deliveryFee = 0L;
    @ApiModelProperty(value = "产品实付金额", example = "100", position = 24)
    private Long actualPrice = 0L;
    @ApiModelProperty(value = "渠道编码", example = "渠道编码", position = 25)
    private String channelCode;
    @ApiModelProperty(value = "渠道名称", example = "渠道名称", position = 26)
    private String channelName;
    @ApiModelProperty(value = "所属大区", example = "所属大区", position = 27)
    private String superOrgName;
    @ApiModelProperty(value = "所属分部", example = "所属分部", position = 28)
    private String orgName;
    @ApiModelProperty(value = "订单平台", example = "订单平台", position = 29)
    private String platformType;
    @ApiModelProperty(value = "平台订单号", example = "平台订单号", position = 30)
    private String platformOrderNum;
    @ApiModelProperty(value = "原始订单", example = "原始订单", position = 31)
    private String originalCode;

    @ApiModelProperty(value = "是否物料", example = "是", position = 70)
    private String orderType;
    @ApiModelProperty(value = "订单备注", example = "订单备注", position = 71)
    private String orderRemark;
}
