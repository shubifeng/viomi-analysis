package com.viomi.analysis.comm.model.dto.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * @Author: luocj
 * @Date: Created in 2018/7/16 15:17
 * @Modified:
 */
@ApiModel(description = "预付款 退货退款报表")
@Data
@ToString
public class OrderRefundPurchaseResp extends OrderRefundResp {

    @ApiModelProperty(value = "原始订单号", example = "", position = 1)
    private String originalOrder;
    @ApiModelProperty(value = "渠道编码", example = "渠道编码", position = 3)
    private String channelCode;
    @ApiModelProperty(value = "渠道名称", example = "渠道名称", position = 4)
    private String channelName;
    @ApiModelProperty(value = "所属大区", example = "所属大区", position = 5)
    private String superOrgName;
    @ApiModelProperty(value = "所属分部", example = "所属分部", position = 6)
    private String orgName;
    @ApiModelProperty(value = "订单平台", example = "订单平台", position = 7)
    private String platformType;
    @ApiModelProperty(value = "平台订单号", example = "平台订单号", position = 8)
    private String platformOrderNum;
    @ApiModelProperty(value = "退货地址", example = "", position = 31)
    private String returnToAddress;
}
