package com.viomi.analysis.comm.constant.report;

/**
 * 发货报表常量
 *
 * @Author: luocj
 * @Date: Created in 2018/5/4 13:01
 * @Modified:
 */

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DeliveryType {
    MIJIA(1, "米家发货报表", "order_delivery_mijia"),
    VIOMI(2, "云米商城发货报表", "order_delivery_viomi"),
    PURCHASE(3, "预付款发货报表", "order_delivery_purchase");

    private Integer type;
    private String desc;
    private String sqlKey;

    public static String getDesc(Integer type){
        for(DeliveryType r: DeliveryType.values()) {
            if (r.getType().equals(type)) {
                return r.getDesc();
            }
        }
        return "未知";
    }

}
