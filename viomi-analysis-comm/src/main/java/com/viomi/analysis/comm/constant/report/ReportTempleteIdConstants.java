package com.viomi.analysis.comm.constant.report;

/**
 * 报表常量模板常量
 *
 * @author yinls
 *         2017年9月29日 下午5:52:59
 */
public class ReportTempleteIdConstants {
    /**
     * 渠道零售订单报表
     */
    public final static Long CHANNEL_SALES_DAY_TEMPLATEID = 21L;// 日报表
    public final static Long CHANNEL_SALES_MONTH_TEMPLATEID = 22L;// 月报表
    public final static Long CHANNEL_SALES_ALL_TEMPLATEID = 23L;// 全部报表

    /**
     * 订单商品销售报表
     */
    public final static Long SKU_SALES_DAY_REPORT_TEMPLATEID = 24L;// 日报表
    public final static Long SKU_SALES_MONTH_REPORT_TEMPLATEID = 25L;// 月报表
    public final static Long SKU_SALES_YEAR_REPORT_TEMPLATEID = 26L;// 年报表

    /**
     * 订单来源报表
     */
    public final static Long ORDER_SOURCE_DAY_REPORT_TEMPLATEID = 27L;

}
