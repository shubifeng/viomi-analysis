package com.viomi.analysis.comm.constant.report;

import com.viomi.base.common.restapi.IBaseEnum;
import lombok.Getter;

/**
 * 开票主体
 *
 * @Author: luocj
 * @Date: Created in 2018/2/6 18:33
 * @Modified:
 */
public enum InvoiceObject implements IBaseEnum {
    UNKNOWN(-100, "-"),
    NORMAL(1, "个人"),
    OFFICIAL(2, "公司");

    @Getter
    private int code;
    @Getter
    private String desc;

    InvoiceObject(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public static InvoiceObject getByCode(byte code){
        InvoiceObject ret = UNKNOWN;
        for(InvoiceObject i : InvoiceObject.values()){
            if(i.code == code){
                ret = i;
                break;
            }
        }
        return ret;
    }

    @Override
    public String getName() {
        return String.valueOf(desc);
    }
}
