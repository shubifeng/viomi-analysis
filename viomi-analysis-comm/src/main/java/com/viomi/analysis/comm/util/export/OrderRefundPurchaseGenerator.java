package com.viomi.analysis.comm.util.export;

import com.viomi.analysis.comm.constant.POIConstants;
import com.viomi.analysis.comm.model.dto.resp.OrderRefundPurchaseResp;
import com.viomi.analysis.comm.util.export.genrator.ExportGenerator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellUtil;

import java.util.Optional;

/**
 * 预付款 退货退款报表 导出实现类
 *
 * @Author: luocj
 * @Date: Created in 2018/4/27 18:16
 * @Modified:
 */
public class OrderRefundPurchaseGenerator extends ExportGenerator {
    @Override
    public Class getEntityClass() {
        return OrderRefundPurchaseResp.class;
    }

    @Override
    public void buildRow( Object obj, Row r, POIConstants pct ) {
        OrderRefundPurchaseResp val = (OrderRefundPurchaseResp) obj;

        int cellNum = 0;

        /*严格按照配置position顺序*/

        //原始订单号
        CellUtil.createCell(r, cellNum++, val.getOriginalOrder(), null);
        //订单号
        CellUtil.createCell(r, cellNum++, val.getOrderCode(), null);
        //渠道编码
        CellUtil.createCell(r, cellNum++, val.getChannelCode(), null);
        //渠道名称
        CellUtil.createCell(r, cellNum++, val.getChannelName(), null);
        //所属大区
        CellUtil.createCell(r, cellNum++, val.getSuperOrgName(), null);
        //所属分部
        CellUtil.createCell(r, cellNum++, val.getOrgName(), null);
        //订单平台
        CellUtil.createCell(r, cellNum++, val.getPlatformType(), null);
        //平台订单号
        CellUtil.createCell(r, cellNum++, val.getPlatformOrderNum(), null);
        //退货入库日期
        CellUtil.createCell(r, cellNum++, val.getConfirmGoodsTime(), null);
        //订单时间
        CellUtil.createCell(r, cellNum++, val.getOrderTime(), null);
        //退款类型
        CellUtil.createCell(r, cellNum++, val.getServiceType(), null);
        //退款状态
//        CellUtil.createCell(r, cellNum++, val.getServiceStatus(), null);
        //商品69码
        CellUtil.createCell(r, cellNum++, val.getBarCode(), null);
        //商品名称
        CellUtil.createCell(r, cellNum++, val.getSkuName(), null);
        //客户姓名
        CellUtil.createCell(r, cellNum++, val.getCustomerName(), null);
        //手机号码
        CellUtil.createCell(r, cellNum++, val.getCustomerPhone(), null);
        //收货地址
        CellUtil.createCell(r, cellNum++, val.getCustomerAddress(), null);
        //退货数量
        CellUtil.createCell(r, cellNum++, String.valueOf(val.getRefundQuantity()), null);
        //收款金额
        Long paymentPrice = Optional.ofNullable(val.getPaymentPrice()).orElse(0L);
        CellUtil.createCell(r, cellNum++, String.valueOf(DECIMAL_FORMAT.format((double) paymentPrice / 100)), pct.getFloatStyle2());
        //退款金额 源数据单位是元
        Long refundPrice = Optional.ofNullable(val.getRefundPrice()).orElse(0L);
        CellUtil.createCell(r, cellNum++, String.valueOf(DECIMAL_FORMAT.format((double) refundPrice)), pct.getFloatStyle2());
        //退款原因
        CellUtil.createCell(r, cellNum++, val.getRefundReason(), null);
        //退回物流单号
        CellUtil.createCell(r, cellNum++, val.getReturnExpressNumber(), null);
        //退回物流公司
        CellUtil.createCell(r, cellNum++, val.getReturnExpressCompany(), null);
        //退货地址
        CellUtil.createCell(r, cellNum++, val.getReturnToAddress(), null);
    }
}
