package com.viomi.analysis.comm.model.dto.resp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author Shubifeng
 */
@ApiModel(description = "退货退款报表")
@Data
@ToString
public class OrderRefundResp implements Serializable {

    @ApiModelProperty(value = "商品ID", example = "1")
    @JsonIgnore
    private String skuId;
    @ApiModelProperty(value = "订单ID", example = "1")
    @JsonIgnore
    private String orderId;

    @ApiModelProperty(value = "订单号", example = "654321", position = 2)
    private String orderCode;
    @ApiModelProperty(value = "退货入库日期", example = "2018-01-01", position = 10)
    private String confirmGoodsTime;
    @ApiModelProperty(value = "订单时间", example = "2018-01-01", position = 11)
    private String orderTime;
    @ApiModelProperty(value = "退款类型", example = "退款", position = 12)
    private String serviceType;
    //    @ApiModelProperty(value = "退款状态", example = "已完结", position = 13)
//    private String serviceStatus;
    @ApiModelProperty(value = "商品69码", example = "69231856011112", position = 14)
    private String barCode;
    @ApiModelProperty(value = "商品名称", example = "商品名称", position = 15)
    private String skuName;
    @ApiModelProperty(value = "客户姓名", example = "", position = 16)
    private String customerName;
    @ApiModelProperty(value = "手机号码", example = "", position = 17)
    private String customerPhone;
    @ApiModelProperty(value = "收货地址", example = "", position = 18)
    private String customerAddress;
    @ApiModelProperty(value = "退货数量", example = "1", position = 19)
    private Long refundQuantity = 0L;
    @ApiModelProperty(value = "收款金额", example = "100", position = 20)
    private Long paymentPrice = 0L;
    @ApiModelProperty(value = "退款金额", example = "100", position = 21)
    private Long refundPrice = 0L;
    @ApiModelProperty(value = "退款原因", example = "", position = 22)
    private String refundReason;
    @ApiModelProperty(value = "退回物流单号", example = "", position = 23)
    private String returnExpressNumber;
    @ApiModelProperty(value = "退回物流公司", example = "", position = 24)
    private String returnExpressCompany;
}
