package com.viomi.analysis.comm.constant.report;

/**
 * refer to vstore PurchaseOrderStatus.java
 */
public enum PurchaseOrderStatus
{
    UNKNOWN((byte) -100, "无法识别"),
    //未付款
    UNPAID((byte) 0, "未付款"),
    // 待发货
    SUBMITED((byte) 1, "待发货"),
    // 已发货
    DELIVERED((byte) 2, "已发货"),
    // 待审核
    APPROVAL((byte) 3, "待仓库审核"),
    // 待退款
    REFUND((byte) 4, "待退款"),
    // 已拆单
    SPLIT((byte) 5, "已拆单"),
    
    // 交付分仓发货
    WAREHOUSE((byte) 6, "交付分仓发货"),
    
    // 分仓退单受理
    WAREHOUSE_APPROVAL((byte) 7, "分仓退单受理"),
    
    // 待销管审核
    MARKETING_ADMIN_APPROVAL((byte) 8, "待销管审核"),
    
    // 已取消
    CANCELED((byte) -1, "已取消");
    
    private Byte code;
    
    private String desc;
    
    private PurchaseOrderStatus(Byte code, String desc)
    {
        this.code = code;
        this.desc = desc;
    }
    
    public static PurchaseOrderStatus getByCode(byte code)
    {
        PurchaseOrderStatus s = UNKNOWN;
        for(PurchaseOrderStatus e : PurchaseOrderStatus.values())
        {
            if(e.code == code)
            {
                s = e;
                break;
            }
        }
        return s;
    }
    
    public Byte getCode()
    {
        return code;
    }
    
    public String getDesc()
    {
        return desc;
    }
}
