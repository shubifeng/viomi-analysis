package com.viomi.analysis.comm.constant;

/**
 * Created by asus on 2018/4/4.
 */
public enum SourceType {
    SHOP_WX("shop-wx", "微信商城"),
    SHOP_APP("shop-app", "app商城"),
    SHOP_YFX("shop-yfx", "云分销"),
    SHOP_MIJIA("shop-mijia", "米家商城"),
    SHOP_CS("shop-cs", "工单"),
    HUODONG("huodong", "活动");
    private String type;
    private String desc;

    SourceType(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public String getType() {
        return String.valueOf(type);
    }


}
