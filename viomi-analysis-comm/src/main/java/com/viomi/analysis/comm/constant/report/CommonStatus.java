package com.viomi.analysis.comm.constant.report;

//refer to vstore CommonStatus.java
public enum CommonStatus
{
    UNKNOWN((byte) -100, "无法识别"),
    FORBIDDEN((byte) -1, "禁用"),
    UNUSED((byte) 0, "未使用"),
    NORMAL((byte) 1, "正常"),
    APPROVAL((byte) 2, "审核中"),
    APPROVED((byte) 3, "审核通过"),
    REJECTED((byte) 4, "审核不通过"),
    UNTREATED((byte) 5, "未处理"),
    PROCESSED((byte) 6, "已处理"),
    DISABLE((byte) 7, "已禁用"),
    ENABLE((byte) 8, "已启用"),
    DELETE((byte) 9, "已删除");
    
    private Byte code;
    
    private String desc;
    
    private CommonStatus(Byte code, String desc)
    {
        this.code = code;
        this.desc = desc;
    }
    
    public static CommonStatus getByCode(Byte code)
    {
        CommonStatus s = UNKNOWN;
        if(code != null)
        {
            for(CommonStatus e : CommonStatus.values())
            {
                if(e.code.equals(code))
                {
                    s = e;
                    break;
                }
            }
        }
        return s;
    }
    
    public Byte getCode()
    {
        return code;
    }
    
    public String getDesc()
    {
        return desc;
    }
}
