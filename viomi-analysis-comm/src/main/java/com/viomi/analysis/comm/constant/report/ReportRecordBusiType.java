package com.viomi.analysis.comm.constant.report;


import java.util.Objects;

public enum ReportRecordBusiType {
    UNKNOWN((int) -100, "无法识别"), //
    EXPRESS_INFO_SKU((int) 1, "发货单sku明细记录"), //
    REFUND_PAYMENT((int) 2, "退款单记录"), //
    ADVANCE_ORDER_SKU((int) 3, "预付款订单sku产品明细记录"), //
    REFUND_ADVANCE_ORDER_SKU((int) 4, "预付款订单退单sku明细记录"), //
    REFUND_CS_ORDER((int) 5, "退货服务单"), //
    CHANNEL_MONTHLY_REMAINDER_ACCOUNT((int) 6, "渠道月度账户剩余记录"), //
    CHANNEL_SALES_REPORT((int) 11, "渠道零售报表记录"), //
    CAMPAIGN_REPORT((int) 101, "活动参与人员记录"),

    ORDER_SKU_SALES((int) 201, "云分销商品销售报表"),
    ORDER_SOURCE_STAT((int) 202, "订单来源统计报表"),
    MANUALLY((int) 10000, "手工补录");

    private Integer code;

    private String desc;

    private ReportRecordBusiType(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static ReportRecordBusiType getByCode(Integer code) {
        ReportRecordBusiType s = UNKNOWN;
        if (code != null) {
            for (ReportRecordBusiType e : ReportRecordBusiType.values()) {
                if (Objects.equals(e.code, code)) {
                    s = e;
                    break;
                }
            }
        }
        return s;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}