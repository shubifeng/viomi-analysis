package com.viomi.analysis.comm.constant.report;

import com.viomi.base.common.restapi.IBaseEnum;
import lombok.Getter;

/**
 * 发票类型
 *
 * @Author: luocj
 * @Date: Created in 2018/2/6 18:33
 * @Modified:
 */
public enum InvoiceType implements IBaseEnum {
    UNKNOWN(-100, "-"),
    NORMAL(1, "普通发票"),
    OFFICIAL(2, "增值税发票");

    @Getter
    private int code;
    @Getter
    private String desc;

    InvoiceType(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public static InvoiceType getByCode(byte code){
        InvoiceType ret = UNKNOWN;
        for(InvoiceType i : InvoiceType.values()){
            if(i.code == code){
                ret = i;
                break;
            }
        }
        return ret;
    }

    @Override
    public String getName() {
        return String.valueOf(desc);
    }
}
