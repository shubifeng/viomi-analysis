package com.viomi.analysis.comm.constant;

import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;

public class POIConstants
{
    private static POIConstants instance;
    
    private Workbook wb;
    
    private Font font1;
    
    private Font font2;
    
    private Font font_red;;
    
    private CellStyle head;
    
    private CellStyle contentAutoWrap;
    
    private CellStyle contentText;
    
    private CellStyle contentFloat;
    
    private CellStyle contentFloat2;
    
    private CellStyle contentDateTime;
    
    private CellStyle contentWarn;
    
    private CellStyle percent;
    
    private CellStyle wrapText;
    
    private DataFormat df;
    
    public POIConstants(Workbook wb2)
    {
        this.wb = wb2;
    }
    
    void init()
    {
        df = wb.createDataFormat();
        
        font1 = wb.createFont();
        font2 = wb.createFont();
        font1.setFontHeightInPoints((short) 12);
        font1.setColor(IndexedColors.BLACK.getIndex());
        font1.setBoldweight(Font.BOLDWEIGHT_NORMAL);
        
        font2.setFontHeightInPoints((short) 10);
        font2.setColor(IndexedColors.BLACK.getIndex());
        
        font_red = wb.createFont();
        font_red.setFontHeightInPoints((short) 10);
        font_red.setColor(IndexedColors.RED.getIndex());
        
        head = wb.createCellStyle();
        head.setBorderBottom(CellStyle.BORDER_THICK);
        head.setBorderLeft(CellStyle.BORDER_THICK);
        head.setBorderRight(CellStyle.BORDER_THICK);
        head.setBorderTop(CellStyle.BORDER_THICK);
        head.setFillBackgroundColor(IndexedColors.BRIGHT_GREEN.getIndex());
        head.setFillForegroundColor(IndexedColors.BRIGHT_GREEN.getIndex());
        head.setFillPattern(CellStyle.FINE_DOTS);
        head.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        head.setAlignment(CellStyle.ALIGN_CENTER);
        head.setDataFormat(df.getFormat("text"));
        head.setFont(font1);
        
        contentText = wb.createCellStyle();
        
        contentAutoWrap = wb.createCellStyle();
        contentAutoWrap.setFont(font2);
        contentAutoWrap.setWrapText(true);
        
        contentFloat = wb.createCellStyle();
        contentFloat.setFont(font2);
        contentFloat.setDataFormat(df.getFormat("#,##0.0"));
        
        contentFloat2 = wb.createCellStyle();
        contentFloat2.setFont(font2);
        contentFloat2.setDataFormat(df.getFormat("#,##0.00"));
        
        contentDateTime = wb.createCellStyle();
        contentDateTime.setFont(font2);
        contentDateTime.setDataFormat(df.getFormat("yyyy-mm-dd h:mm:ss"));
        
        contentWarn = wb.createCellStyle();
        contentWarn.setFont(font_red);
        
        percent = wb.createCellStyle();
        percent.setFont(font2);
        percent.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00%"));
        
        wrapText = wb.createCellStyle();
        wrapText.setWrapText(true);
    }
    
    public CellStyle getContentAutoWrap()
    {
        return contentAutoWrap;
    }
    
    public void setContentAutoWrap(CellStyle contentAutoWrap)
    {
        this.contentAutoWrap = contentAutoWrap;
    }
    
    public CellStyle getFloatStyle()
    {
        return this.contentFloat;
    }
    
    public CellStyle getHeadStyle()
    {
        return this.head;
    }
    
    public CellStyle getDateTimeStyle()
    {
        return this.contentDateTime;
    }
    
    public CellStyle getWarnStyle()
    {
        return this.contentWarn;
    }
    
    public CellStyle getPercentStyle()
    {
        return percent;
    }
    
    public CellStyle getWrapStyle()
    {
        return wrapText;
    }
    
    public CellStyle getFloatStyle2()
    {
        return contentFloat2;
    }
    
    public static class POIConstantsInstanceFactory
    {
        public static POIConstants initWithWorkbook(Workbook wb)
        {
            POIConstants instance = new POIConstants(wb);
            instance.init();
            return instance;
        }
    }
}
