package com.viomi.analysis.comm.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

/**
 * refer to vstore MathUtil.java
 */
public class MathUtil
{
    
    /**
     * @return a+b
     */
    public static <T extends Number> Double add(T a, T b)
    {
        BigDecimal x = new BigDecimal(String.valueOf(a));
        BigDecimal y = new BigDecimal(String.valueOf(b));
        return x.add(y).doubleValue();
    }
    
    /**
     * @return a-b
     */
    public static <T extends Number> Double subtract(T a, T b)
    {
        BigDecimal x = new BigDecimal(String.valueOf(a));
        BigDecimal y = new BigDecimal(String.valueOf(b));
        return x.subtract(y).doubleValue();
    }
    
    /**
     * @return a*b
     */
    public static <T extends Number> Double multiply(T a, T b)
    {
        BigDecimal x = new BigDecimal(String.valueOf(a));
        BigDecimal y = new BigDecimal(String.valueOf(b));
        return x.multiply(y).doubleValue();
    }
    
    /**
     * 保留2位小数
     * 
     * @return a/b
     */
    public static <T extends Number> Double divide(T a, T b)
    {
        return divide(a, b, 2);
    }
    
    /**
     * @param scale 保留几位小数
     * @return a/b
     */
    public static <T extends Number> Double divide(T a, T b, int scale)
    {
        BigDecimal x = new BigDecimal(String.valueOf(a));
        BigDecimal y = new BigDecimal(String.valueOf(b));
        return x.divide(y, scale, RoundingMode.HALF_UP).doubleValue();
    }
    
    /**
     * 精确价格比例计算
     * 
     * @param totalPrice 总成交价
     * @param prePrice 单个报价
     * @return 单个折扣后价钱
     * @author:tanghui@viomi.com.cn
     * @createtime:2017年12月11日 下午4:12:20
     * @description:
     */
    public static Map<Long, Long> scalePrice(Long totalPrice, Map<Long, Long> prePrice)
    {
        Map<Long, Long> ret = new HashMap<>();
        Long totalPrePrice = 0L;
        for(Map.Entry<Long, Long> entry : prePrice.entrySet())
        {
            totalPrePrice += entry.getValue();
        }
        
        BigDecimal tppb = new BigDecimal(totalPrePrice);
        BigDecimal tpb = new BigDecimal(totalPrice);
        BigDecimal discountScale = tpb.divide(tppb, 2, 4);
        int size = prePrice.size();
        int index = 0;
        Long remain = totalPrice;
        for(Map.Entry<Long, Long> entry : prePrice.entrySet())
        {
            if(index == size - 1)
            {
                ret.put(entry.getKey(), remain);
            }
            else
            {
                BigDecimal afterDiscount = discountScale.multiply(new BigDecimal(entry.getValue()));
                Long afl = afterDiscount.longValue();
                remain = remain - afl;
                ret.put(entry.getKey(), afl);
            }
            index++;
        }
        
        return ret;
    }
    
    public static void main(String[] args)
    {
        // System.out.println(519.6D + 129.9D);
        // System.out.println(add(519.6D, 129.9D));
        // System.out.println(519.6D - 129.9D);
        // System.out.println(subtract(519.6D, 129.9D));
        // System.out.println(multiply(1.3D, 2.64D));
        // System.out.println(divide(1.3D, 2.64D));
        // System.out.println(divide(1.3D, 2.64D, 3));
        
//        Double pofitScale = MathUtil.divide(333.00D,
//                MathUtil.subtract(26474.00D, 480.00D), 8);
//        Double profitPay = MathUtil.multiply(MathUtil.subtract(17316D, 320D), pofitScale);
//        profitPay = new BigDecimal(profitPay).setScale(2, RoundingMode.HALF_EVEN).doubleValue();
//        Double totalPay = 17316D;
//        Double depositPay = MathUtil.subtract(totalPay, profitPay);
//        System.out.println(profitPay);
//        System.out.println(depositPay);
//        
//        Double profitPay2 = MathUtil.multiply(MathUtil.subtract(9158D, 160D), pofitScale);
//        profitPay2 = new BigDecimal(profitPay2).setScale(2, RoundingMode.HALF_EVEN).doubleValue();
//        Double totalPay2 = 9158D;
//        Double depositPay2 = MathUtil.subtract(totalPay2, profitPay2);
//        System.out.println(profitPay2);
//        System.out.println(depositPay2);

//        double sum1 = 0.1D + 0.2D;
//        double sum2 = add(0.1D, 0.2D);
//        System.out.println(sum1);
//        System.out.println(sum2);
    }
}
