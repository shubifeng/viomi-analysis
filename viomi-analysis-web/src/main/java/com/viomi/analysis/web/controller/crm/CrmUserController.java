package com.viomi.analysis.web.controller.crm;

import com.viomi.analysis.comm.constant.SourceType;
import com.viomi.analysis.comm.model.dto.req.UserSearchReq;
import com.viomi.analysis.comm.model.dto.resp.UniqueUserResp;
import com.viomi.analysis.service.gp.user.UserService;
import com.viomi.analysis.web.BaseUserController;
import com.viomi.base.common.constant.Req;
import com.viomi.base.common.util.ResultResp;
import com.viomi.boot.myjdbc.page.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by asus on 2018/3/29.
 */
@Api(tags = "crm-用户融合", description = "http://120.92.208.154:8880/projects/CPRW/issues/CPRW-2607?filter=allopenissues")
@RequestMapping(value = "crm/users")
@RestController
@Validated
@Slf4j
public class CrmUserController implements BaseUserController {

    @Autowired
    private UserService userService;


    @GetMapping(value = "")
    @ApiOperation(value = "用户列表")
    public ResultResp<PageInfo<UniqueUserResp>> orderChart(
            @ApiParam(defaultValue = "1")
            @RequestParam(value = Req.pageNum) Integer pageNum,
            @ApiParam(defaultValue = "20")
            @RequestParam(value = Req.pageSize) Integer pageSize,
            @ApiParam(value = "用户来源,SHOP_WX(微信商城),SHOP_YFX(云分销)，SHOP_APP(app商城),SHOP_MIJIA(米家),SHOP_CS(工单),HUODONG(活动)") @RequestParam(required = false) SourceType sourceType, @ApiParam(value = "用户名") @RequestParam(required = false) String userName,
            @ApiParam(value = "联系电话,值为@时表示查询不存在手机号码的用户记录") @RequestParam(required = false) String phone,
            @ApiParam(value = "云米账号,值为@时表示查询不存在云米账号的用户记录") @RequestParam(required = false) String viomiNo,
            @ApiParam(value = "注册时间,格式:2017-07-07 12:00:00|2017-09-09 12:30:30,值为@时表示查询非注册的用户记录") @RequestParam(required = false) String queryTime
    ) {
        UserSearchReq rq = new UserSearchReq();
        if (sourceType != null) {
            rq.setSourceType(sourceType);
        }
        rq.setPageNum(pageNum);
        rq.setPageSize(pageSize);
        rq.setUserName(userName);
        rq.setPhone(phone);
        rq.setViomiNo(viomiNo);
        rq.setQueryTime(queryTime);
        return ResultResp.ok(userService.list(rq));
    }


    @GetMapping(value = "/shenji")
    @ApiOperation(value = "审计用户列表")
    public ResultResp<PageInfo<UniqueUserResp>> orderChart(
            @ApiParam(value = "页码") @RequestParam int pageNum,
            @ApiParam(value = "每页行数") @RequestParam int pageSize,
            @ApiParam(value = "来源类型") @RequestParam SourceType sourceType,
            @ApiParam(value = "查询时间以|分割,格式2017-09-09 23:23:10|2018-09-09 23:23:10") @RequestParam String queryTime,
            @ApiParam(value = "用户姓名") @RequestParam String userName,
            @ApiParam(value = "联系电话") @RequestParam String phone,
            @ApiParam(value = "微信昵称") @RequestParam String nickname) {
        return ResultResp.ok(userService.listShenJiUser(pageNum, pageSize, sourceType.getType(), userName, phone, nickname, queryTime));
    }

}
