package com.viomi.analysis.web.controller.report;

import com.viomi.analysis.comm.model.dto.resp.WaresSalesDetailResp;
import com.viomi.analysis.comm.model.dto.resp.WaresSalesResp;
import com.viomi.analysis.comm.model.dto.resp.WaresStockResp;
import com.viomi.analysis.service.gp.wares.WaresService;
import com.viomi.analysis.web.BaseUserController;
import com.viomi.base.common.constant.PlatformTypeConst;
import com.viomi.base.common.constant.Req;
import com.viomi.base.common.constant.TimeTypeConst;
import com.viomi.base.common.model.ParamDate;
import com.viomi.base.common.util.ResultResp;
import com.viomi.base.common.util.date.DateUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * @author Shubifeng
 * @date 2017-2-10
 */
@Api(tags = "云分销app-商品数据", description = "http://120.92.208.154:8880/browse/CPRW-2117")
@RestController
@RequestMapping(value = "reports/wares")
@Validated
@Slf4j
public class ReportWareController implements BaseUserController {

    @Autowired
    private WaresService waresService;


    @ApiOperation(value = "商品的销售量,销售额", notes = "订单数据暂未剔除发生部分退款的商品")
    @GetMapping("sales")
    public ResultResp<List<WaresSalesResp>> waresSalesList(
            @ApiParam(value = Req.platformTypeDesc, required = true)
            @NotNull
            @RequestParam(value = Req.platformType) PlatformTypeConst platformType,
            @NotNull
            @ApiParam(value = Req.timeDesc, required = true)
            @RequestParam(value = Req.timeType) TimeTypeConst timeType,
            @RequestParam(value = Req.beginTime) long beginTime,
            @RequestParam(value = Req.endTime) long endTime) {
        ParamDate paramDate = DateUtils.convert(timeType, beginTime, endTime);
        List<WaresSalesResp> list = waresService.listWaresSales(getOrgId(), getChannelId(), platformType, paramDate);
        return ResultResp.ok(list);
    }

    @ApiOperation(value = "商品的销售量,销售额明细", notes = "具体到每种商品sku维度的数据,数据暂未剔除部分退款的商品")
    @GetMapping("salesDetail")
    public ResultResp<List<WaresSalesDetailResp>> waresSalesDetailList(
            @ApiParam(value = Req.platformTypeDesc, required = true)
            @NotNull
            @RequestParam(value = Req.platformType) PlatformTypeConst platformType,
            @NotNull
            @ApiParam(value = Req.timeDesc, required = true)
            @RequestParam(value = Req.timeType) TimeTypeConst timeType,
            @RequestParam(value = Req.beginTime) long beginTime,
            @RequestParam(value = Req.endTime) long endTime) {
        ParamDate paramDate = DateUtils.convert(timeType, beginTime, endTime);
        //List<WaresSalesDetailResp> list = waresService.listWaresSalesDetail(null, null, platformType, paramDate);
        List<WaresSalesDetailResp> list = waresService.listWaresSalesDetail(getOrgId(), getChannelId(), platformType, paramDate);
        return ResultResp.ok(list);
    }

    @ApiOperation(value = "商品的库存量,待发货数", notes = "商品所属类目catalogIds api: http://dwz.cn/7H6Njt")
    @GetMapping("stock")
    public ResultResp<List<WaresStockResp>> wareProdList(
            @Length(max = 20)
            @RequestParam(required = false) String prodName,
            @ApiParam(value = "商品在售状态：0预售,1在售,null时全部")
            @Range(min = 0, max = 1, message = "商品在售状态必须是 0 或 1 或 null")
            @RequestParam(required = false) Integer saleStatus,
            @ApiParam(value = "商品所属类目id：多个以,分割", example = "1,32,234")
            @RequestParam(required = false) String catalogIds,
            @Pattern(regexp = "\\s||\\d*\\,\\d*", flags = {})
            @ApiParam(value = "库存范围：10,200 表示(开始值,结束值),或者为空值", example = "132,234")
            @RequestParam(required = false) String stockCountRange,
            @Pattern(regexp = "\\s||\\d*\\,\\d*", flags = {})
            @ApiParam(value = "待出库范围：10,200 表示(开始值,结束值),或者为空值", example = "132,234")
            @RequestParam(required = false) String pendingCountRange) {
        List<WaresStockResp> list = waresService.listWaresStock(prodName, saleStatus, catalogIds, stockCountRange, pendingCountRange);
        return ResultResp.ok(list);
    }
}
