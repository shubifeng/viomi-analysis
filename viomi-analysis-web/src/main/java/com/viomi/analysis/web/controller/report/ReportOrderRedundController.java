package com.viomi.analysis.web.controller.report;

import com.viomi.analysis.comm.constant.report.RefundType;
import com.viomi.analysis.comm.model.dto.resp.OrderRefundPurchaseResp;
import com.viomi.analysis.comm.model.dto.resp.OrderRefundViomiResp;
import com.viomi.analysis.service.gp.fd.OrderRefundService;
import com.viomi.base.common.constant.Req;
import com.viomi.base.common.constant.TimeTypeConst;
import com.viomi.base.common.model.ParamDate;
import com.viomi.base.common.util.ResultResp;
import com.viomi.base.common.util.date.DateUtils;
import com.viomi.boot.myjdbc.page.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

/**
 * 云分销-退货退款报表
 *
 * @Author: luocj
 * @Date: Created in 2018/3/26 15:13
 * @Modified:
 */
@Api(tags = "云分销-退货退款报表", description = "http://120.92.208.154:8880/browse/CPRW-3567")
@RequestMapping(value = "reports/refund")
@RestController
@Validated
@Slf4j
public class ReportOrderRedundController {
    @Autowired
    private OrderRefundService orderRefundService;

    @ApiOperation(value = "云米商城-退货退款报表")
    @GetMapping(value = {"viomi"})
    public ResultResp<PageInfo<OrderRefundViomiResp>> refundViomi(
            @NotNull
            @ApiParam(required = true, defaultValue = "1538323200")
            @RequestParam(value = Req.beginTime) Long beginTime,
            @NotNull
            @ApiParam(required = true, defaultValue = "1541001599")
            @RequestParam(value = Req.endTime) Long endTime,
            @ApiParam(defaultValue = "1")
            @RequestParam(value = Req.pageNum) Integer pageNum,
            @ApiParam(defaultValue = "20")
            @RequestParam(value = Req.pageSize) Integer pageSize ) {

        //转换为按月查询
        ParamDate paramDate = DateUtils.convert(TimeTypeConst.month, beginTime, endTime);
        //查询列表数据
        PageInfo<OrderRefundViomiResp> pageInfo = orderRefundService.listRefundViomi(paramDate.getBegintime(), paramDate.getEndtime(), pageNum, pageSize);
        return ResultResp.ok(pageInfo);
    }

    @ApiOperation(value = "预付款-退货退款报表")
    @GetMapping(value = {"purchase"})
    public ResultResp<PageInfo<OrderRefundPurchaseResp>> refundPurchase(
            @NotNull
            @ApiParam(required = true, defaultValue = "1538323200")
            @RequestParam(value = Req.beginTime) Long beginTime,
            @NotNull
            @ApiParam(required = true, defaultValue = "1541001599")
            @RequestParam(value = Req.endTime) Long endTime,
            @ApiParam(defaultValue = "1")
            @RequestParam(value = Req.pageNum) Integer pageNum,
            @ApiParam(defaultValue = "20")
            @RequestParam(value = Req.pageSize) Integer pageSize ) {

        //转换为按月查询
        ParamDate paramDate = DateUtils.convert(TimeTypeConst.month, beginTime, endTime);
        //查询列表数据
        PageInfo<OrderRefundPurchaseResp> pageInfo = orderRefundService.listRefundPurchase(paramDate.getBegintime(), paramDate.getEndtime(), pageNum, pageSize);
        return ResultResp.ok(pageInfo);
    }

    @ApiOperation(value = "退货退款报表导出")
    @GetMapping(value = {"export"})
    public ResultResp deliverExport(
            HttpServletRequest request,
            HttpServletResponse response,
            @NotNull
            @ApiParam(required = true)
            @RequestParam(value = "refundType") Integer refundType,
            @NotNull
            @ApiParam(required = true, defaultValue = "1538323200")
            @RequestParam(value = Req.beginTime) Long beginTime,
            @NotNull
            @ApiParam(required = true, defaultValue = "1541001599")
            @RequestParam(value = Req.endTime) Long endTime,
            @ApiParam(defaultValue = "1")
            @RequestParam(value = Req.pageNum) Integer pageNum,
            @ApiParam(defaultValue = "20")
            @RequestParam(value = Req.pageSize) Integer pageSize,
            @ApiParam(defaultValue = "1")
            @RequestParam(value = "isExportAll") Integer isExportAll ) {

        //转换为按月查询
        ParamDate paramDate = DateUtils.convert(TimeTypeConst.day, beginTime, endTime);
        Workbook wb = orderRefundService.getRefundWorkbook(refundType, paramDate, pageNum, pageSize, isExportAll);

        CommonExport.dealExportResult(request, response, refundType, RefundType.getDesc(refundType), beginTime, endTime, wb);
        return ResultResp.ok();
    }


}
