package com.viomi.analysis.web.controller.report;

import com.viomi.analysis.comm.constant.report.DeliveryType;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * 公共导出类
 *
 * @Author: luocj
 * @Date: Created in 2018/7/24 19:32
 * @Modified:
 */
@Slf4j
public class CommonExport {

    public static void dealExportResult( HttpServletRequest request, HttpServletResponse response, Integer busiType, String busiDesc, Long beginTime, Long endTime, Workbook wb ) {
        OutputStream out = null;
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
            String fileName = String.format("%s-%s_%s.xlsx", formatter.format(LocalDateTime.ofInstant(Instant.ofEpochSecond(beginTime), ZoneId.systemDefault())),
                    formatter.format(LocalDateTime.ofInstant((Instant.ofEpochSecond(endTime)), ZoneId.systemDefault())),
                    DeliveryType.getDesc(busiType));

            String userAgent = request.getHeader("User-Agent");
            // IE或IE内核或Edge
            if (userAgent.contains("MSIE") || userAgent.contains("Trident") || userAgent.contains("Edge")) {
                fileName = URLEncoder.encode(fileName, "UTF-8");
            } else {
                // 非IE
                fileName = new String(fileName.getBytes(), "ISO-8859-1");
            }

            response.setCharacterEncoding("UTF-8");
            response.setHeader("Content-Disposition", String.format("attachment;filename=%s", fileName));
            response.setContentType("application/msexcel");
            out = response.getOutputStream();
            if (null != wb) {
                wb.write(out);
            }
        } catch (Exception e) {
            log.error("Export msexcel error: ", e);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (wb != null) {
                    wb.close();
                }
            } catch (Exception e) {
                log.error("Export close error: ", e.getMessage());
            }
        }
    }
}
