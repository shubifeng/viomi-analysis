package com.viomi.analysis.web.controller.report;

import com.viomi.analysis.comm.model.dto.resp.ChannelPurchaseOrderChartDTO;
import com.viomi.analysis.comm.model.dto.resp.ChannelPurchaseOrderDetailResp;
import com.viomi.analysis.comm.model.dto.resp.ChannelPurchaseOrderResp;
import com.viomi.analysis.comm.model.dto.resp.ChannelPurchaseOrderTotalDTO;
import com.viomi.analysis.service.gp.channel.ChannelPurchaseService;
import com.viomi.analysis.web.BaseUserController;
import com.viomi.base.common.constant.Req;
import com.viomi.base.common.constant.TimeTypeConst;
import com.viomi.base.common.model.ParamDate;
import com.viomi.base.common.util.ResultResp;
import com.viomi.base.common.util.date.DateUtils;
import com.viomi.boot.myjdbc.page.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 预付款提货报表
 *
 * @author luocj
 * @Date Created in 2018/1/29 17:27
 * @Modified
 */
@Api(tags = "云分销app-预付款报表", description = "http://120.92.208.154:8880/browse/CPRW-2117")
@RequestMapping(value = "reports/channelPurchase")
@RestController
@Validated
@Slf4j
public class ReportChannelPurchaseController implements BaseUserController {
    @Autowired
    private ChannelPurchaseService channelPurchaseService;

    @ApiOperation(value = "预付款提货汇总柱状图")
    @GetMapping(value = "orderChart")
    public ResultResp<ChannelPurchaseOrderChartDTO> orderChart(
            @ApiParam(value = Req.purchaseOrderDesc)
            @RequestParam(value = Req.purchaseOrderType, defaultValue = "1") Integer orderType,
            @ApiParam(value = Req.timeDesc, required = true)
            @NotNull
            @RequestParam(value = Req.timeType) TimeTypeConst timeType,
            @NotNull
            @ApiParam(required = true)
            @RequestParam(value = Req.beginTime) Long beginTime,
            @NotNull
            @ApiParam(required = true)
            @RequestParam(value = Req.endTime) Long endTime) {

        //日期转换
        ParamDate paramDate = DateUtils.convert(timeType, beginTime, endTime);
        //查询列表数据
        List<ChannelPurchaseOrderResp> listDTO = channelPurchaseService.listOrderForChart(orderType, paramDate);
        //汇总数据
        ChannelPurchaseOrderTotalDTO totalDTO = new ChannelPurchaseOrderTotalDTO();
        listDTO.stream().forEach(row -> {
//            totalDTO.setOrderNum(totalDTO.getOrderNum() + row.getOrderNum());
//            totalDTO.setOrderFee(totalDTO.getOrderFee() + row.getOrderFee());
//            totalDTO.setRefundOrderNum(totalDTO.getRefundOrderNum() + row.getRefundOrderNum());
//            totalDTO.setRefundOrderFee(totalDTO.getRefundOrderFee() + row.getRefundOrderFee());
            totalDTO.setOrderNum(Math.addExact(totalDTO.getOrderNum(), row.getOrderNum()));
            totalDTO.setOrderFee(Math.addExact(totalDTO.getOrderFee(), row.getOrderFee()));
            totalDTO.setRefundOrderNum(Math.addExact(totalDTO.getRefundOrderNum(), row.getRefundOrderNum()));
            totalDTO.setRefundOrderFee(Math.addExact(totalDTO.getRefundOrderFee(), row.getRefundOrderFee()));
        });
        //组装并返回
        ChannelPurchaseOrderChartDTO chartDTO = new ChannelPurchaseOrderChartDTO(totalDTO, listDTO);
        return ResultResp.ok(chartDTO);
    }

    @ApiOperation(value = "预付款提货报表明细")
    @GetMapping(value = "orderChartDetail")
    public ResultResp<PageInfo<ChannelPurchaseOrderDetailResp>> orderChartDetail(
            @ApiParam(value = Req.purchaseOrderDesc)
            @NotNull
            @RequestParam(value = Req.purchaseOrderType, defaultValue = "1") Integer orderType,
            @NotNull
            @RequestParam(value = Req.beginTime) Long beginTime,
            @NotNull
            @RequestParam(value = Req.endTime) Long endTime,
            @ApiParam(defaultValue = "1")
            @RequestParam(value = Req.pageNum) Integer pageNum,
            @ApiParam(defaultValue = "20")
            @RequestParam(value = Req.pageSize) Integer pageSize) {

        PageInfo<ChannelPurchaseOrderDetailResp> pageInfo = channelPurchaseService.listOrderForChartDetail(orderType, beginTime, endTime, pageNum, pageSize);
        return ResultResp.ok(pageInfo);
    }

}
