package com.viomi.analysis.web.controller.report;

import com.viomi.analysis.comm.constant.report.DeliveryType;
import com.viomi.analysis.comm.model.dto.resp.OrderDeliveryMijiaResp;
import com.viomi.analysis.comm.model.dto.resp.OrderDeliveryPurchaseResp;
import com.viomi.analysis.comm.model.dto.resp.OrderDeliveryViomiResp;
import com.viomi.analysis.service.gp.fd.OrderDeliveryService;
import com.viomi.base.common.constant.Req;
import com.viomi.base.common.constant.TimeTypeConst;
import com.viomi.base.common.model.ParamDate;
import com.viomi.base.common.util.ResultResp;
import com.viomi.base.common.util.date.DateUtils;
import com.viomi.boot.myjdbc.page.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

/**
 * 云分销-发货报表
 *
 * @Author: luocj
 * @Date: Created in 2018/3/26 15:13
 * @Modified:
 */
@Api(tags = "云分销-发货报表", description = "http://120.92.208.154:8880/browse/KFRW-6002")
@RequestMapping(value = "reports/delivery")
@RestController
@Validated
@Slf4j
public class ReportOrderDeliveryController {
    @Autowired
    private OrderDeliveryService orderDeliveryService;

    @ApiOperation(value = "米家发货报表")
    @GetMapping(value = {"mijia"})
    public ResultResp<PageInfo<OrderDeliveryMijiaResp>> deliverMijia(
            @NotNull
            @ApiParam(required = true)
            @RequestParam(value = Req.beginTime, defaultValue = "1514736000") Long beginTime,
            @NotNull
            @ApiParam(required = true, defaultValue = "1517414399")
            @RequestParam(value = Req.endTime) Long endTime,
            @ApiParam(defaultValue = "1")
            @RequestParam(value = Req.pageNum) Integer pageNum,
            @ApiParam(defaultValue = "20")
            @RequestParam(value = Req.pageSize) Integer pageSize ) {

        //转换为按月查询
        ParamDate paramDate = DateUtils.convert(TimeTypeConst.month, beginTime, endTime);
        //查询列表数据
        PageInfo<OrderDeliveryMijiaResp> pageInfo = orderDeliveryService.listDeliveryMijia(paramDate.getBegintime(), paramDate.getEndtime(), pageNum, pageSize);
        return ResultResp.ok(pageInfo);
    }

    @ApiOperation(value = "云米商城发货报表")
    @GetMapping(value = {"viomi"})
    public ResultResp<PageInfo<OrderDeliveryViomiResp>> deliverViomi(
            @NotNull
            @ApiParam(required = true)
            @RequestParam(value = Req.beginTime, defaultValue = "1514736000") Long beginTime,
            @NotNull
            @ApiParam(required = true, defaultValue = "1517414399")
            @RequestParam(value = Req.endTime) Long endTime,
            @ApiParam(defaultValue = "1")
            @RequestParam(value = Req.pageNum) Integer pageNum,
            @ApiParam(defaultValue = "20")
            @RequestParam(value = Req.pageSize) Integer pageSize ) {

        //转换为按月查询
        ParamDate paramDate = DateUtils.convert(TimeTypeConst.month, beginTime, endTime);
        //查询列表数据
        PageInfo<OrderDeliveryViomiResp> pageInfo = orderDeliveryService.listDeliveryViomi(paramDate.getBegintime(), paramDate.getEndtime(), pageNum, pageSize);
        return ResultResp.ok(pageInfo);
    }

    @ApiOperation(value = "预付款发货报表")
    @GetMapping(value = {"purchase"})
    public ResultResp<PageInfo<OrderDeliveryPurchaseResp>> deliverPurchase(
            @NotNull
            @ApiParam(required = true, defaultValue = "1514736000")
            @RequestParam(value = Req.beginTime) Long beginTime,
            @NotNull
            @ApiParam(required = true, defaultValue = "1517414399")
            @RequestParam(value = Req.endTime) Long endTime,
            @ApiParam(defaultValue = "1")
            @RequestParam(value = Req.pageNum) Integer pageNum,
            @ApiParam(defaultValue = "20")
            @RequestParam(value = Req.pageSize) Integer pageSize ) {

        //转换为按月查询
        ParamDate paramDate = DateUtils.convert(TimeTypeConst.month, beginTime, endTime);
        //查询列表数据
        PageInfo<OrderDeliveryPurchaseResp> pageInfo = orderDeliveryService.listDeliveryPurchase(paramDate.getBegintime(), paramDate.getEndtime(), pageNum, pageSize);
        return ResultResp.ok(pageInfo);
    }

    @ApiOperation(value = "发货报表导出")
    @GetMapping(value = {"export"})
    public ResultResp deliverExport(
            HttpServletRequest request,
            @NotNull
            @ApiParam(required = true)
            @RequestParam(value = "refundType") Integer deliverType,
            @NotNull
            @ApiParam(required = true, defaultValue = "1514736000")
            @RequestParam(value = Req.beginTime) Long beginTime,
            @NotNull
            @ApiParam(required = true, defaultValue = "1517414399")
            @RequestParam(value = Req.endTime) Long endTime,
            @ApiParam(defaultValue = "1")
            @RequestParam(value = Req.pageNum) Integer pageNum,
            @ApiParam(defaultValue = "20")
            @RequestParam(value = Req.pageSize) Integer pageSize,
            @ApiParam(defaultValue = "1")
            @RequestParam(value = "isExportAll") Integer isExportAll,
            HttpServletResponse response ) {

        //转换为按月查询
        ParamDate paramDate = DateUtils.convert(TimeTypeConst.day, beginTime, endTime);
        Workbook wb = orderDeliveryService.getDeliveryWorkbook(deliverType, paramDate, pageNum, pageSize, isExportAll);

        CommonExport.dealExportResult(request, response, deliverType, DeliveryType.getDesc(deliverType), beginTime, endTime, wb);
        return ResultResp.ok();
    }

}
