package com.viomi.analysis.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by asus on 2018/1/4.
 */
@FeignClient(url = "https://vj.viomi.com.cn/services2", name = "loguetownapi")
public interface TestApi {

    /**
     * 发起查询调用
     *
     * @param token
     * @return
     */
    @GetMapping("channel/report/salesReportNew/eachLevel.json")
    String test(@RequestParam("token") String token);


}
