package com.viomi.analysis.org.exception;

import com.viomi.base.common.constant.CommRespCode;
import com.viomi.base.common.exception.CommonException;
import com.viomi.base.common.util.ResultResp;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 错误信息拦截格式化输出（springboot，Controller以内的调用所有异常捕获）
 * @author ShuBifeng
 */
@Slf4j
@RestControllerAdvice
public class ExceptionHandle {

    /**
     * feign遠程調用失敗
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = FeignException.class)
    public ResponseEntity<Object> handle(FeignException e, HttpServletRequest request) {
        ResultResp resultResp = ResultResp.fail(CommRespCode.HTTP_EXCEPTION);
        log.error("返回对象: {}", resultResp);
        log.error("错误信息: {}", e);
        return ResponseEntity.badRequest().body(serverError(request, resultResp));
    }

    /**
     * 内部错误
     *
     * @param e
     * @param request
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Object> handle(Exception e, HttpServletRequest request) {
        ResultResp respDefaultErr = ResultResp.fail(CommRespCode.PROGRAM_EXCEPTION);
        if (e instanceof MethodArgumentNotValidException) {
            BindingResult bindingResult = ((MethodArgumentNotValidException) e).getBindingResult();
            FieldError err = bindingResult.getFieldError();
            respDefaultErr.setDesc(err.getField() + "," + err.getDefaultMessage());
        } else if (e instanceof org.springframework.web.HttpRequestMethodNotSupportedException) {
            respDefaultErr.setDesc("http method不适用");
        } else if (e instanceof org.springframework.web.HttpMediaTypeNotSupportedException) {
            respDefaultErr.setDesc("content type不适用");
        } else if (e instanceof org.springframework.web.bind.MissingServletRequestParameterException) {
            MissingServletRequestParameterException e1 = ((MissingServletRequestParameterException) e);
            respDefaultErr.setDesc(e1.getParameterName() + " 参数缺失");
        } else if (e instanceof org.springframework.validation.BindException) {
            BindingResult bindingResult = ((BindException) e).getBindingResult();
            FieldError err = bindingResult.getFieldError();
            respDefaultErr.setDesc(err.getField() + "," + err.getDefaultMessage());
        } else if (e instanceof javax.validation.ConstraintViolationException) {
            ConstraintViolationException e2 = (ConstraintViolationException) e;
            for (ConstraintViolation<?> s : e2.getConstraintViolations()) {
                respDefaultErr.setDesc(StringUtils.split(s.getPropertyPath().toString(), ".")[1] + "," + s.getMessage());
                break;
            }
        } else if (e instanceof HttpMessageNotReadableException) {
            respDefaultErr.setDesc("request body is missing");
        } else if (e instanceof MethodArgumentTypeMismatchException) {
            MethodArgumentTypeMismatchException e2 = (MethodArgumentTypeMismatchException) e;
            respDefaultErr.setDesc(String.format("%s的值%s无效", e2.getName(), e2.getValue()));
        } else if (e instanceof IllegalArgumentException) {
            respDefaultErr.setDesc(e.getMessage());
        } else {
            log.error("error: {}", e.getMessage());
        }
        log.error("返回对象: {}", respDefaultErr);
        log.error("错误信息: {}", e);
        return ResponseEntity.badRequest().body(serverError(request, respDefaultErr));
    }

    /**
     * 正常返回
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = CommonException.class)
    public ResponseEntity<Object> handle(CommonException e) {
        ResultResp resultResp = ResultResp.fail(e.getCommCode());
        log.error("返回对象: {}", resultResp);
        log.error("错误信息: {}", e);
        return ResponseEntity.ok().body(resultResp);
    }

    /**
     * 非200时，数据结构尽量跟原始异常信息结构保存一致
     *
     * @param resultErr
     * @return
     */
    private Map<String, Object> serverError(HttpServletRequest request, ResultResp resultErr) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put("timestamp", resultErr.getTimestamp());
        map.put("status", resultErr.getCode());
        map.put("path", request.getRequestURI());
        map.put("error", resultErr.getDesc());
        return map;
    }

}
