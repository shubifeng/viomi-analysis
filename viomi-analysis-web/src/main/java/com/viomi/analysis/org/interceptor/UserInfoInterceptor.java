package com.viomi.analysis.org.interceptor;

import com.viomi.analysis.service.gp.RedisService;
import com.viomi.analysis.web.BaseUserController;
import com.viomi.base.common.constant.Constants;
import com.viomi.base.model.dto.user.CacheUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * @author ShuBifeng
 */
@Slf4j
@Component
public class UserInfoInterceptor extends HandlerInterceptorAdapter implements BaseUserController {

    @Autowired
    private RedisService redisService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        //1.获取用户的token
        String token = request.getParameter(Constants.TOKEN);
        log.debug("请求进入--token: [{}] value:[{}]", Constants.TOKEN, token);
        if (Objects.nonNull(token)) {
            //转换为小写
            token = token.toLowerCase();
            //2.拉取用户
            boolean isLogin = redisService.existCacheUser(token);
            setToken(token);
            if (isLogin) {
                CacheUser cacheUser = redisService.getCacheUser(token);
                setCacheUser(cacheUser);
                setCurrUserId(cacheUser.getUserId());
            }
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
                                Exception exception) throws Exception {
        //在整个请求结束之后被调用，也就是在DispatcherServlet 渲染了对应的视图之后执行（主要是用于进行资源清理工作）
        remove();
        log.debug("请求结束---销毁");
    }

}
