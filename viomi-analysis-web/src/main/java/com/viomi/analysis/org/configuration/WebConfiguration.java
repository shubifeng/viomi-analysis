package com.viomi.analysis.org.configuration;


import com.spring4all.swagger.EnableSwagger2Doc;
import com.viomi.analysis.org.interceptor.LoginInterceptor;
import com.viomi.analysis.org.interceptor.UserInfoInterceptor;
import com.viomi.base.common.restapi.StringToBaseEnumConverterFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * <p>function:
 * <p>User: LeeJohn
 * <p>Date: 2016/7/18
 * <p>Version: 1.0
 */
@EnableSwagger2Doc
@Configuration
@Slf4j
//@DisconfFile(filename = "application-default.yml")
public class WebConfiguration extends WebMvcConfigurerAdapter {

    @Autowired
    private UserInfoInterceptor userInfoInterceptor;
    @Autowired
    private LoginInterceptor loginInterceptor;

    @Autowired
    private Environment env;

    @Override
    public void addInterceptors( InterceptorRegistry registry ) {
        //配置需要验证用户信息的环境:dev-开发;product-生产
        log.info("环境标识：{}", env.getActiveProfiles());
        boolean isNoNeedLogin = env.acceptsProfiles("dev");
        if (!isNoNeedLogin) {
            registry.addInterceptor(userInfoInterceptor).addPathPatterns("/**");
            registry.addInterceptor(loginInterceptor).addPathPatterns("/**").excludePathPatterns("/login/*", "/swagger-resources/**", "/docs/*", "/druid/*");
//            registry.addInterceptor(loginInterceptor).addPathPatterns("/**")
//                    .excludePathPatterns("/login", "/register", "/sendTelCode", "/forgetPassWord","/checkImageCode", "/getImageCode","/telIsExist","/personal/*", "/swagger-resources/**","/company/*",
//                            "/org/company/*");company
            ///*registry.addInterceptor(loginInterceptor).addPathPatterns("*//**").pathMatcher(pathMatcher)
            // .excludePathPatterns("/login", "/register","/sendTelCode","/updatePassWord","/imageCode*//*");*/
        }
        super.addInterceptors(registry);
    }

    @Override
    public void addFormatters( FormatterRegistry registry ) {
        registry.addConverterFactory(new StringToBaseEnumConverterFactory());
    }

    /**
     * 静态资源映射
     *
     * @param registry 静态资源注册器
     */
    @Override
    public void addResourceHandlers( ResourceHandlerRegistry registry ) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        super.addResourceHandlers(registry);
    }

}
