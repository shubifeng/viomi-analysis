package com.viomi.analysis.repository.gp;

import com.viomi.analysis.comm.constant.report.CommonConstants;
import com.viomi.boot.myjdbc.core.MyJdbcNamedModel;
import com.viomi.boot.myjdbc.core.MyJdbcTemplate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * TODO
 *
 * @Author: luocj
 * @Date: Created in 2018/2/7 13:48
 * @Modified:
 */
@Repository
public class DivisionRepository {
    @Autowired
    private MyJdbcTemplate gpJdbcTemplate;

    /**
     * 通过divisionCode获取地区完整名称（省市区）
     * @param divisionCode
     * @return
     */
    public String getDivisionFullName(Object divisionCode){
        StringBuffer fullName = new StringBuffer();
        if (null != divisionCode) {
            String divisionCodes = "'"+ divisionCode.toString()+"'";
            divisionCodes += ",'"+ StringUtils.substring(divisionCode.toString(), 0, 2)+"0000'";
            divisionCodes += ",'"+StringUtils.substring(divisionCode.toString(), 0, 4)+"00'";
            List<Map<String, Object>> divisionInfos = queryDivisionInfos(divisionCodes, CommonConstants.DIVISION_VERSION);
            if (CollectionUtils.isNotEmpty(divisionInfos)) {
                Object province = "";
                Object city = "";
                Object district = "";
                for(Map r: divisionInfos){
                    if (null != r && CommonConstants.DIVISION_LEVEL_PROVINCE == r.get("level")) {
                        province = Optional.ofNullable(r.get("name")).orElse("");
                    } else if (null != r && CommonConstants.DIVISION_LEVEL_CITY == r.get("level")) {
                        city = Optional.ofNullable(r.get("name")).orElse("");
                    } else if (null != r && CommonConstants.DIVISION_LEVEL_DISTRICT == r.get("level")) {
                        district = Optional.ofNullable(r.get("name")).orElse("");
                    }
                }
                fullName.append(province.toString()).append(city.toString()).append(district.toString());
            }
        }
        return fullName.toString();
    }

    /**
     * 通过divisionCode和版本信息获取 地区信息
     * @param divisionCode
     * @param version
     * @return
     */
    private List queryDivisionInfos(String divisionCode, Byte version) {
        Map<String, Object> map = new HashMap<>();
        StringBuffer sbf = new StringBuffer("select * from vm_common.common_division where 1=1 ");
        if (StringUtils.isNotBlank(divisionCode)) {
            sbf.append(" and code in ("+StringUtils.strip(divisionCode.toString(), "[]")+") ");
        }
        if (null != version && 0 != version) {
            map.put("v_version", version);
            sbf.append(" and version = :v_version ");
        }
        return gpJdbcTemplate.find(new MyJdbcNamedModel(sbf.toString(), map));
    }

}
