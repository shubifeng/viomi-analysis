package com.viomi.analysis.repository.redis;


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.util.CollectionUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 自定义的redis缓存管理器
 * 支持方法上配置过期时间
 *
 * @author: https://github.com/wyh-spring-ecosystem-student/spring-boot-student
 */
public class CustomizedRedisCacheManager extends RedisCacheManager {

    private static final Logger logger = LoggerFactory.getLogger(CustomizedRedisCacheManager.class);
    private RedisCacheManager redisCacheManager = null;
    /**
     * 父类cacheNullValues字段
     */
    private boolean cacheNullValues = true;

    /**
     * 0 - never expire
     */
    private long defaultExpiration = 0;
    private Map<String, Long> cacheTimes = null;

    public CustomizedRedisCacheManager(RedisOperations redisOperations) {
        super(redisOperations);
    }

    /**
     * 获取过期时间
     *
     * @return
     */
    public long getExpirationSecondTime(String name) {
        if (StringUtils.isEmpty(name)) {
            return 0;
        }

        Long cacheTime = null;
        if (!CollectionUtils.isEmpty(cacheTimes)) {
            cacheTime = cacheTimes.get(name);
        }
        Long expiration = cacheTime != null ? cacheTime : defaultExpiration;
        return expiration < 0 ? 0 : expiration;
    }

    /**
     * 创建缓存(设置过期时间等)
     *
     * @param cacheName 缓存名称
     * @return
     */
    @Override
    public CustomizedRedisCache getMissingCache(String cacheName) {
        // 有效时间，初始化获取默认的有效时间
        Long expirationSecondTime = getExpirationSecondTime(cacheName);

        logger.info("Create cacheName: {}, expired time:{}", cacheName, expirationSecondTime);

        return new CustomizedRedisCache(cacheName, (this.isUsePrefix() ? this.getCachePrefix().prefix(cacheName) : null),
                this.getRedisOperations(), expirationSecondTime, cacheNullValues);
    }

    /**
     * 根据缓存名称设置缓存的有效时间，单位秒
     *
     * @param cacheTimes
     */
    public void setCacheTimess(Map<String, Long> cacheTimes) {
        this.cacheTimes = (cacheTimes != null ? new ConcurrentHashMap<String, Long>(cacheTimes) : null);
    }

    /**
     * 设置默认的过去时间， 单位：秒
     *
     * @param defaultExpireTime
     */
    @Override
    public void setDefaultExpiration(long defaultExpireTime) {
        super.setDefaultExpiration(defaultExpireTime);
        this.defaultExpiration = defaultExpireTime;
    }

}
