package com.viomi.analysis.repository.gp;

import com.google.common.collect.Lists;
import com.viomi.analysis.comm.constant.SqlXmlPath;
import com.viomi.boot.myjdbc.core.MyJdbcTemplate;
import com.viomi.boot.myjdbc.util.SqlMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by asus on 2018/2/6.
 */
@Repository
public class ProdCatalogRepository {

    private final static Map<String, String> WARES_CATALOG = SqlMap.loadQueries(SqlXmlPath.WARES_CATALOG);

    @Autowired
    private MyJdbcTemplate gpJdbcTemplate;

    public Iterable<Long> getIds(String code) {
        String sql = WARES_CATALOG.get("wares_catalog_child_by_code");
        List<Long> listIds = gpJdbcTemplate.findSingleColumnValue("id", sql, code);
        List<Long> ids = Lists.newArrayListWithCapacity(10);
        listIds.parallelStream().forEach(aLong -> {
            ids.addAll(listIds(aLong));
        });
        return ids;
    }

    public List<Long> listIds(Long id) {
        String sql = WARES_CATALOG.get("wares_catalog_child_by_id");
        return gpJdbcTemplate.findSingleColumnValue("id", sql, id);
    }
}

