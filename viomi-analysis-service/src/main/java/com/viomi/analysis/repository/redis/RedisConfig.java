package com.viomi.analysis.repository.redis;

import com.alibaba.fastjson.parser.ParserConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * # 自定义Redis缓存
 * select.cache:
 *   # 缓存名称，多个用逗号隔开
 *   names: ChannelCache,TestCache
 *   # 过期时间，大于0不能为空;不配置默认1天（24*60*60=86400秒）;
 *   timeout: 86400
 *
 * @author: https://github.com/wyh-spring-ecosystem-student/spring-boot-student
 */
@Configuration
public class RedisConfig {

    private final String CACHENAME_SEPARATOR = ",";
    // redis缓存的有效时间[单位是秒]
    @Value("${redis.default.expiration:86400}")
    private long redisDefaultExpiration;
    // 缓存有效时间[单位是秒]
    @Value("${select.cache.names:}")
    private String selectCacheNames;
    // 缓存有效时间[单位是秒]
    @Value("${select.cache.timeout:43200}")
    private long selectCacheTimeout;

    /**
     * 重写Redis序列化方式，使用Json方式: 自定义配置RedisTemplate并指定Serializer
     * 数据存储到Redis的时候，键（key）和值（value）都是通过Spring提供的Serializer序列化到数据库的。
     * RedisTemplate默认使用的是JdkSerializationRedisSerializer，StringRedisTemplate默认使用的是StringRedisSerializer。
     * Spring Data JPA提供了以下Serializer：
     * GenericToStringSerializer、Jackson2JsonRedisSerializer、JacksonJsonRedisSerializer、JdkSerializationRedisSerializer、OxmSerializer、StringRedisSerializer。
     *
     * @param redisConnectionFactory
     * @return
     */
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);

        // 全局开启AutoType，不建议使用
        // ParserConfig.getGlobalInstance().setAutoTypeSupport(true);
        // 建议使用这种方式，小范围指定白名单
        ParserConfig.getGlobalInstance().addAccept("com.viomi.analysis.model.dto.");

//        /**
//         * 设置键（key）的序列化:
//         *  StringRedisSerializer: StringXX是转为String，JacksonXX是将对象转为json。
//         *  -注意Key使用了StringRedisSerializer，那么Key只能是String类型的，不能为long, int等基本类型，否则会报错抛异常(ClassCastException)。
//         *  GenericToStringSerializer: 指定Object.class,支持更多类型
//         */
//        GenericToStringSerializer genericToStringSerializer = new GenericToStringSerializer(Object.class);
//        redisTemplate.setKeySerializer(genericToStringSerializer);
//        redisTemplate.setHashKeySerializer(genericToStringSerializer);
//
////        // 设置值（value）的序列化采用自定义FastJsonRedisSerializer
//        FastJsonRedisSerializer fastJsonRedisSerializer = new FastJsonRedisSerializer(Object.class);
//        redisTemplate.setValueSerializer(fastJsonRedisSerializer);
//        redisTemplate.setHashValueSerializer(fastJsonRedisSerializer);

        /**
         * 1.需要jackson-databind支持
         * 2.new RedisCache时cacheNullValues需设置为false(java.lang.IllegalArgumentException)
         * - 20L存储格式:["java.lang.Long",20]
         */
//        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
//        ObjectMapper om = new ObjectMapper();
//        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
//        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
//        jackson2JsonRedisSerializer.setObjectMapper(om);
//        redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
//        redisTemplate.setHashValueSerializer(jackson2JsonRedisSerializer);

        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }

    /**
     * 重写RedisCacheManager的getCache方法，实现设置key的有效时间
     *
     * @param redisTemplate
     * @return
     */
    @Bean
    public RedisCacheManager cacheManager(RedisTemplate<String, Object> redisTemplate) {
        CustomizedRedisCacheManager redisCacheManager = new CustomizedRedisCacheManager(redisTemplate);
        // 开启使用缓存名称最为key前缀
        redisCacheManager.setUsePrefix(true);
        //这里可以设置一个默认的过期时间 单位是秒
        redisCacheManager.setDefaultExpiration(redisDefaultExpiration);

        // 设置缓存的过期时间和自动刷新时间
        Map<String, Long> cacheTimes = new HashMap<>();
        if (StringUtils.isNotBlank(selectCacheNames)) {
            String[] cacheNames = selectCacheNames.split(CACHENAME_SEPARATOR);
            for(String r: cacheNames) {
                cacheTimes.put(r, selectCacheTimeout);
            }
        }
        redisCacheManager.setCacheTimess(cacheTimes);

        return redisCacheManager;
    }

}

