package com.viomi.analysis.repository.gp;

import com.viomi.analysis.comm.constant.SqlXmlPath;
import com.viomi.analysis.comm.constant.report.CommonConstants;
import com.viomi.analysis.comm.model.dto.resp.ChannelPurchaseOrderDetailResp;
import com.viomi.analysis.comm.model.entity.gp.ChannelBaseInfoEntity;
import com.viomi.boot.myjdbc.core.MyJdbcNamedModel;
import com.viomi.boot.myjdbc.core.MyJdbcTemplate;
import com.viomi.boot.myjdbc.util.SqlMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 渠道公共服务类
 *
 * @Author: luocj
 * @Date: Created in 2018/2/5 16:26
 * @Modified:
 */
@Repository
@Slf4j
public class ChannelRepository {
    @Autowired
    private MyJdbcTemplate gpJdbcTemplate;

    private final static Map<String, String> SQL_MAP = SqlMap.loadQueries(SqlXmlPath.WARES);

    /**
     * 通过渠道id查询渠道信息
     *
     * @param channelId
     * @param status
     * @return
     */
    public ChannelBaseInfoEntity queryChannelBaseInfoById( Object channelId, Byte status ) {

        Map<String, Object> map = new HashMap<>();
        StringBuffer sbf = new StringBuffer("select * from vm_channel.channel_base_info where 1=1 ");

        if (channelId != null) {
            map.put("v_channelId", channelId);
            sbf.append(" and id = :v_channelId ");
        }
        if (null != status && 0 != status) {
            map.put("v_status", status);
            sbf.append(" and status = :v_status ");
        }
        MyJdbcNamedModel myJdbcNamedModel = new MyJdbcNamedModel(sbf.toString(), map);
        return gpJdbcTemplate.findUnique(ChannelBaseInfoEntity.class, myJdbcNamedModel);
    }


    /**
     * 通过递归脚本 获取子节点
     *
     * @param channelId
     * @param channelName
     * @param orgId
     * @param status
     * @return
     */
    public List<Long> getSubChannelIdsByRecursive( Long channelId, String channelName, Long orgId, Byte status ) {
        Map<String, Object> map = new HashMap<>();
        map.put("v_channel_id", channelId);
        StringBuffer sbf = new StringBuffer(SQL_MAP.get("get_sub_channel_ids_by_recursive") + " where 1=1 ");

        if (StringUtils.isNotBlank(channelName)) {
            map.put("v_channelName", "%" + channelName + "%");
            sbf.append(" and name like :v_channelName ");
        }
        if (orgId != null) {
            map.put("v_orgId", orgId);
            sbf.append(" and org_id = :v_orgId ");
        }
        if (null != status && 0 != status) {
            map.put("v_status", status);
            sbf.append(" and status = :v_status ");
        }
        MyJdbcNamedModel myJdbcNamedModel = new MyJdbcNamedModel(sbf.toString(), map);
        return gpJdbcTemplate.findSingleColumnValue("id", myJdbcNamedModel);
    }

    /**
     * 获取子节点
     *
     * @param channelId
     * @param channelName
     * @param orgId
     * @param status
     * @return
     */
    public List<Long> getSubChannelIds( Long channelId, String channelName, Long orgId, Byte status ) {
        Map<String, Object> map = new HashMap<>();
        StringBuffer sbf = new StringBuffer("select id from vm_channel.channel_base_info where 1=1 ");

        if (channelId != null) {
            List<Long> allChannelIds = getChannelIdsByRoot(channelId, status);
            sbf.append(" and id in (" + StringUtils.strip(allChannelIds.toString(), "[]") + ") ");
        }
        if (StringUtils.isNotBlank(channelName)) {
            map.put("v_channelName", "%" + channelName + "%");
            sbf.append(" and name like :v_channelName ");
        }
        if (orgId != null) {
            map.put("v_orgId", orgId);
            sbf.append(" and org_id = :v_orgId ");
        }
        if (null != status && 0 != status) {
            map.put("v_status", status);
            sbf.append(" and status = :v_status ");
        }
        MyJdbcNamedModel myJdbcNamedModel = new MyJdbcNamedModel(sbf.toString(), map);
        return gpJdbcTemplate.findSingleColumnValue("id", myJdbcNamedModel);

    }

    /**
     * 通过渠道id查询所有子节点id
     *
     * @param channelId
     * @param status
     * @return
     */
    public List<Long> getChannelIdsByRoot( Long channelId, Byte status ) {
        List<Long> channelIds = new ArrayList<>();
        if (channelId != null) {
            channelIds.add(channelId);
            queryChildChannelIds(channelId, channelIds, null, status);
        }
        return channelIds;
    }

    /**
     * 递归函数 查询子渠道
     *
     * @param root
     * @param ids
     * @param type
     * @param status
     */
    private void queryChildChannelIds( Object root, List<Long> ids, Byte type, Byte status ) {
        List<Map<String, Object>> childs = queryChannelBaseInfos(root, type, status);
        if (childs != null && childs.size() > 0) {
            for (Map<String, Object> child : childs) {
                queryChildChannelIds(child.get("id"), ids, type, status);
                if (type == null || (type != null && child.get("type").equals(type))) {
                    ids.add(Long.parseLong(child.get("id").toString()));
                }
            }
        }
    }

    /**
     * 通过条件查询渠道信息
     *
     * @param parentId
     * @param type
     * @param status
     * @return
     */
    public List<Map<String, Object>> queryChannelBaseInfos( Object parentId, Byte type, Byte status ) {
        Map<String, Object> map = new HashMap<>();
        StringBuffer sbf = new StringBuffer("select parent_id,id,type,status,org_id from vm_channel.channel_base_info where 1=1 ");
        if (null != parentId) {
            map.put("v_parentId", parentId);
            sbf.append(" and parent_id = :v_parentId ");
        }
        if (null != type && 0 != type) {
            map.put("v_type", type);
            sbf.append(" and type = :v_type ");
        }
        if (null != status && 0 != status) {
            map.put("v_status", status);
            sbf.append(" and status = :v_status ");
        }
        return gpJdbcTemplate.find(new MyJdbcNamedModel(sbf.toString(), map));
    }

    @Deprecated
//    @Cacheable(value = "ChannelCache", key = "'getAllChannelName:'+#channelId", condition = "#channelId != null", sync = true)
    public ChannelPurchaseOrderDetailResp getAllChannelName( Long channelId ) {
        ChannelPurchaseOrderDetailResp retChannelNames = new ChannelPurchaseOrderDetailResp();
        String rootChannel = "*";
        String secondChannel = "*";
        String channelName = "*";
        if (null == channelId) {
            return retChannelNames;
        }
        log.info("ChannelCache for key getAllChannelName:" + channelId);

        ChannelBaseInfoEntity channelInfo = queryChannelBaseInfoById(channelId, null);
        //城市运营
        if (null != channelInfo && null != channelInfo.getParentId() && CommonConstants.SUPER_CHANNEL_ID.equals(channelInfo.getParentId())) {
            rootChannel = channelInfo.getName();
        } else if (null != channelInfo && null != channelInfo.getParentId()) {
            //经销商
            ChannelBaseInfoEntity pChannelInfo = queryChannelBaseInfoById(channelInfo.getParentId(), null);
            if (null != pChannelInfo.getParentId() && CommonConstants.SUPER_CHANNEL_ID.equals(pChannelInfo.getParentId())) {
                rootChannel = pChannelInfo.getName();
                secondChannel = channelInfo.getName();
            } else if (null != pChannelInfo && null != pChannelInfo.getParentId()) {
                //门店
                ChannelBaseInfoEntity ppChannelInfo = queryChannelBaseInfoById(pChannelInfo.getParentId(), null);
                if (null != ppChannelInfo.getParentId() && CommonConstants.SUPER_CHANNEL_ID.equals(ppChannelInfo.getParentId())) {
                    rootChannel = ppChannelInfo.getName();
                    secondChannel = pChannelInfo.getName();
                    channelName = channelInfo.getName();
                } else {
                    channelName = "*";
                }
            }
        }

        retChannelNames.setCityAgentName(rootChannel);
        retChannelNames.setRegionalAgentName(secondChannel);
        retChannelNames.setTerminalName(channelName);
        return retChannelNames;
    }
}
