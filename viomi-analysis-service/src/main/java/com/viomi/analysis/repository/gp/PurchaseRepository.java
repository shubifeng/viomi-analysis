package com.viomi.analysis.repository.gp;

import com.viomi.analysis.comm.constant.report.CommonStatus;
import com.viomi.analysis.comm.constant.report.PurchaseOrderAttrCode;
import com.viomi.boot.myjdbc.core.MyJdbcNamedModel;
import com.viomi.boot.myjdbc.core.MyJdbcTemplate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * TODO
 *
 * @Author: luocj
 * @Date: Created in 2018/2/7 13:47
 * @Modified:
 */
@Repository
public class PurchaseRepository {
    @Autowired
    private MyJdbcTemplate gpJdbcTemplate;

    /**
     * 通过指定条件 获取预付款属性信息
     *
     * @param orderId
     * @param expressNo
     * @return
     */
    public List<Map<String, Object>> queryChannelPurchaseOrderAttr(Object orderId, String expressNo) {
        Map<String, Object> map = new HashMap<>();
        StringBuffer sbf = new StringBuffer("select * from vm_channel.channel_purchase_order_attr where 1=1 ");
        if (null != orderId) {
            map.put("v_orderId", orderId);
            sbf.append(" and order_Id = :v_orderId ");
        }
        if (StringUtils.isNotBlank(expressNo)) {
            map.put("v_expressNo", expressNo.trim());
            sbf.append(" and value = :v_expressNo and code = '" + PurchaseOrderAttrCode.EXPRESS_NUMBER.getCode() + "'");
        }
        sbf.append(" and status = ").append(CommonStatus.NORMAL.getCode());
        return gpJdbcTemplate.find(new MyJdbcNamedModel(sbf.toString(), map));
    }

    /**
     * 通过产品名称 获取相关skuIds
     *
     * @param productName
     * @return
     */
    public List<Long> getProdSkuIdsByName(String productName) {
        Map<String, Object> map = new HashMap<>();
        StringBuffer sbf = new StringBuffer("select id from vm_wares.prod_sku where 1=1 ");
        if (StringUtils.isNotBlank(productName)) {
            map.put("v_productName", "%" + productName + "%");
            sbf.append(" and name like :v_productName ");
        }
        sbf.append(" and status = ").append(CommonStatus.NORMAL.getCode());
        MyJdbcNamedModel myJdbcNamedModel = new MyJdbcNamedModel(sbf.toString(), map);
        return gpJdbcTemplate.findSingleColumnValue("id", myJdbcNamedModel);
    }

    /**
     * 通过预付款订单id获取 订单商品信息
     *
     * @param orderId
     * @return
     */
    public List<Map<String, Object>> getPurchaseOrderSkus(Object orderId) {
        Map<String, Object> map = new HashMap<>();
        StringBuffer sbf = new StringBuffer("select s.sku_id,k.name,s.order_id,round(s.standard_price::numeric/100::numeric,2) as standard_price,s.quantity,round(s.standard_price*s.quantity::numeric/100::numeric,2) as sum_price " +
                "from vm_channel.channel_purchase_order_sku s,vm_wares.prod_sku k where s.sku_id=k.id ");
        if (null != orderId) {
            map.put("v_orderId", orderId);
            sbf.append(" and s.order_id = :v_orderId");
        }
        return gpJdbcTemplate.find(new MyJdbcNamedModel(sbf.toString(), map));
    }

    /**
     * 通过预付款订单id获取 订单支付信息
     *
     * @param orderId
     * @return
     */
    public Map<String, Object> getPurchaseOrderPayment(Object orderId) {
        Map<String, Object> map = new HashMap<>();
//        StringBuffer sbf = new StringBuffer("select * from vm_channel.channel_purchase_order_payment where ");
        StringBuffer sbf = new StringBuffer("select round(delivery_fee::numeric/100::numeric,2) as delivery_fee,round(pay_price::numeric/100::numeric,2) as pay_price,round(deposit_price::numeric/100::numeric,2) as deposit_price,round(profit_price::numeric/100::numeric,2) as profit_price from vm_channel.channel_purchase_order_payment where 1=1 ");

        if (null != orderId) {
            map.put("v_orderId", orderId);
            sbf.append(" and order_id = :v_orderId");
        }
        sbf.append(" and status = ").append(CommonStatus.NORMAL.getCode());
        return gpJdbcTemplate.findFirstRow(new MyJdbcNamedModel(sbf.toString(), map));
    }

}
