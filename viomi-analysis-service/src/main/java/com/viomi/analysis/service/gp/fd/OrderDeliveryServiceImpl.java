package com.viomi.analysis.service.gp.fd;

import com.viomi.analysis.comm.constant.SqlXmlPath;
import com.viomi.analysis.comm.constant.report.CommonConstants;
import com.viomi.analysis.comm.constant.report.DeliveryType;
import com.viomi.analysis.comm.model.dto.resp.OrderDeliveryMijiaResp;
import com.viomi.analysis.comm.model.dto.resp.OrderDeliveryPurchaseResp;
import com.viomi.analysis.comm.model.dto.resp.OrderDeliveryViomiResp;
import com.viomi.analysis.comm.util.ModelUtil;
import com.viomi.analysis.comm.util.export.OrderDeliveryMijiaGenerator;
import com.viomi.analysis.comm.util.export.OrderDeliveryPurchaseGenerator;
import com.viomi.analysis.comm.util.export.OrderDeliveryViomiGenerator;
import com.viomi.base.common.model.ParamDate;
import com.viomi.boot.myjdbc.core.MyJdbcPNamedModel;
import com.viomi.boot.myjdbc.core.MyJdbcTemplate;
import com.viomi.boot.myjdbc.page.PageInfo;
import com.viomi.boot.myjdbc.util.SqlMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.List;
import java.util.Map;

/**
 * 发货报表
 *
 * @Author: luocj
 * @Date: Created in 2018/3/27 15:47
 * @Modified:
 */
@Slf4j
@Service
public class OrderDeliveryServiceImpl implements OrderDeliveryService {
    private final static Map<String, String> SQL_MAP = SqlMap.loadQueries(SqlXmlPath.ORDER_DELIVERY);

    @Autowired
    private MyJdbcTemplate gpJdbcTemplate;

    @Override
    public PageInfo<OrderDeliveryMijiaResp> listDeliveryMijia( Long beginTime, Long endTime, Integer pageNum, Integer pageSize ) {
        MyJdbcPNamedModel myJdbcPNamedModel = ModelUtil.getMyJdbcPNamedModel(SQL_MAP.get(DeliveryType.MIJIA.getSqlKey()), beginTime, endTime, pageNum, pageSize);
        return gpJdbcTemplate.page(OrderDeliveryMijiaResp.class, myJdbcPNamedModel);
    }

    @Override
    public PageInfo<OrderDeliveryViomiResp> listDeliveryViomi( Long beginTime, Long endTime, Integer pageNum, Integer pageSize ) {
        MyJdbcPNamedModel myJdbcPNamedModel = ModelUtil.getMyJdbcPNamedModel(SQL_MAP.get(DeliveryType.VIOMI.getSqlKey()), beginTime, endTime, pageNum, pageSize);
        return gpJdbcTemplate.page(OrderDeliveryViomiResp.class, myJdbcPNamedModel);
    }

    @Override
    public PageInfo<OrderDeliveryPurchaseResp> listDeliveryPurchase( Long beginTime, Long endTime, Integer pageNum, Integer pageSize ) {
        MyJdbcPNamedModel myJdbcPNamedModel = ModelUtil.getMyJdbcPNamedModel(SQL_MAP.get(DeliveryType.PURCHASE.getSqlKey()), beginTime, endTime, pageNum, pageSize);
        return gpJdbcTemplate.page(OrderDeliveryPurchaseResp.class, myJdbcPNamedModel);
    }

    @Override
    public Workbook getDeliveryWorkbook( Integer deliverType, ParamDate paramDate, Integer pageNum, Integer pageSize, Integer isExportAll ) {
        StopWatch sw = new StopWatch();

        Workbook wb = null;
        int logCount = 0;
        if (DeliveryType.MIJIA.getType().equals(deliverType)) {
            sw.start("getExportList");
            List<OrderDeliveryMijiaResp> exportList = getExportList(OrderDeliveryMijiaResp.class, SQL_MAP.get(DeliveryType.MIJIA.getSqlKey()), paramDate, isExportAll, pageNum, pageSize);
            logCount = exportList.size();
            sw.stop();
            //生成Excel
            sw.start("buildWorkbook");
            wb = new OrderDeliveryMijiaGenerator().buildWorkbookForSubSheet(exportList);
            sw.stop();
        } else if (DeliveryType.VIOMI.getType().equals(deliverType)) {
            sw.start("getExportList");
            List<OrderDeliveryViomiResp> exportList = getExportList(OrderDeliveryViomiResp.class, SQL_MAP.get(DeliveryType.VIOMI.getSqlKey()), paramDate, isExportAll, pageNum, pageSize);
            logCount = exportList.size();
            sw.stop();
            //生成Excel
            sw.start("buildWorkbook");
            wb = new OrderDeliveryViomiGenerator().buildWorkbookForSubSheet(exportList);
            sw.stop();
        } else if (DeliveryType.PURCHASE.getType().equals(deliverType)) {
            sw.start("getExportList");
            List<OrderDeliveryPurchaseResp> exportList = getExportList(OrderDeliveryPurchaseResp.class, SQL_MAP.get(DeliveryType.PURCHASE.getSqlKey()), paramDate, isExportAll, pageNum, pageSize);
            logCount = exportList.size();
            sw.stop();
            //生成Excel
            sw.start("buildWorkbook");
            wb = new OrderDeliveryPurchaseGenerator().buildWorkbookForSubSheet(exportList);
            sw.stop();
        }
        log.debug("Export data count{}, time[{}-{}], deliverType[{}]", logCount, paramDate.getBegintime(), paramDate.getEndtime(), DeliveryType.getDesc(deliverType));
        log.debug(sw.prettyPrint());

        return wb;
    }

    private <T> List<T> getExportList( Class clazz, String SQL, ParamDate paramDate, Integer isExportAll, Integer pageNum, Integer pageSize ) {
        List exportList;
        if (CommonConstants.YES_OR_NO_YES.equals(isExportAll)) {
            exportList = gpJdbcTemplate.find(clazz, ModelUtil.getMyJdbcNamedModel(SQL, paramDate.getBegintime(), paramDate.getEndtime()));
        } else {
            PageInfo pageInfo = gpJdbcTemplate.page(clazz, ModelUtil.getMyJdbcPNamedModel(SQL, paramDate.getBegintime(), paramDate.getEndtime(), pageNum, pageSize));
            exportList = pageInfo.getList();
        }
        return exportList;
    }

}
