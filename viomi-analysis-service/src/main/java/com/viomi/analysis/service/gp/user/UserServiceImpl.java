package com.viomi.analysis.service.gp.user;

import com.viomi.analysis.comm.constant.SqlXmlPath;
import com.viomi.analysis.comm.model.dto.req.UserSearchReq;
import com.viomi.analysis.comm.model.dto.resp.UniqueUserResp;
import com.viomi.boot.myjdbc.core.MyJdbcPNamedModel;
import com.viomi.boot.myjdbc.core.MyJdbcTemplate;
import com.viomi.boot.myjdbc.page.PageInfo;
import com.viomi.boot.myjdbc.util.SqlMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by asus on 2018/3/29.
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final static Map<String, String> SQL_MAP = SqlMap.loadQueries(SqlXmlPath.USER);
    @Autowired
    private MyJdbcTemplate myJdbcTemplate;

    @Override
    public PageInfo<UniqueUserResp> list( UserSearchReq userSearchReq) {
        MyJdbcPNamedModel myJdbcPModel = new MyJdbcPNamedModel();
        myJdbcPModel.setPageNo(userSearchReq.getPageNum());
        myJdbcPModel.setPageSize(userSearchReq.getPageSize());
        myJdbcPModel.setMapParams(null);
        StringBuilder sb = new StringBuilder(SQL_MAP.get("user_list"));
        sb.append(" where 1=1  ");
        Map<String, Object> m = new HashMap<>();
        String ai = "@";
        if (StringUtils.isNotBlank(userSearchReq.getPhone())) {
            if (Objects.equals(userSearchReq.getPhone(), ai)) {
                sb.append(" and phone is null");
            } else {
                sb.append(" and phone=:phone");
                m.put("phone", userSearchReq.getPhone());
            }
        }
        if (Objects.nonNull(userSearchReq.getSourceType())) {
            sb.append(" and source_type=:sourceType");
            m.put("sourceType", userSearchReq.getSourceType().getType());
        }
        if (StringUtils.isNotBlank(userSearchReq.getUserName())) {
            sb.append(" and user_name=:userName");
            m.put("userName", userSearchReq.getUserName());
        }
        if (StringUtils.isNotBlank(userSearchReq.getViomiNo())) {
            if (Objects.equals(userSearchReq.getViomiNo(), ai)) {
                sb.append(" and vm_no is null");
            } else {
                sb.append(" and vm_no=:viomiNo");
                m.put("viomiNo", userSearchReq.getViomiNo());
            }
        }

        if (StringUtils.isNotBlank(userSearchReq.getQueryTime())) {
            String queryTime = userSearchReq.getQueryTime();
            String[] timeArr = StringUtils.split(queryTime, "|");
            if (Objects.equals(userSearchReq.getQueryTime(), ai)) {
                sb.append(" and reg_time is null");
            } else if (timeArr.length == 2) {
                sb.append(" and reg_time >= :queryTimeStart");
                sb.append(" and reg_time <= :queryTimeEnd");
                DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                m.put("queryTimeStart", LocalDateTime.parse(timeArr[0], df));
                m.put("queryTimeEnd", LocalDateTime.parse(timeArr[1], df));
            }
        }
        sb.append(" order by created_time desc");

        myJdbcPModel.setSql(sb.toString());
        myJdbcPModel.setMapParams(m);
        return myJdbcTemplate.page(UniqueUserResp.class, myJdbcPModel);
    }

    @Override
    public PageInfo<UniqueUserResp> listShenJiUser(int pageNum, int pageSize, String sourceType, String userName, String phone, String nickname, String queryTime) {
        MyJdbcPNamedModel myJdbcPModel = new MyJdbcPNamedModel();
        myJdbcPModel.setPageNo(pageNum);
        myJdbcPModel.setPageSize(pageSize);
        myJdbcPModel.setMapParams(null);
        StringBuilder sb = new StringBuilder("select * from temp_shenji_20180403");
        sb.append(" where 1=1  ");
        Map<String, Object> m = new HashMap<>();
        if (StringUtils.isNotBlank(sourceType)) {
            sb.append(" and source_type=:sourceType");
            m.put("sourceType", sourceType);
        }
        if (StringUtils.isNotBlank(userName)) {
            sb.append(" and user_name ~ :userName");
            m.put("userName", userName);
        }
        if (StringUtils.isNotBlank(phone)) {
            sb.append(" and phone=:phone");
            m.put("phone", phone);
        }
        if (StringUtils.isNotBlank(nickname)) {
            sb.append(" and nickname ~ :nickname");
            m.put("nickname", nickname);
        }

        if (StringUtils.isNotBlank(queryTime)) {
            String[] timeArr = StringUtils.split(queryTime, "|");
            sb.append(" and created_time >= :queryTimeStart");
            sb.append(" and created_time <= :queryTimeEnd");
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            m.put("queryTimeStart", LocalDateTime.parse(timeArr[0], df));
            m.put("queryTimeEnd", LocalDateTime.parse(timeArr[1], df));
        }
        sb.append(" order by created_time desc");

        myJdbcPModel.setSql(sb.toString());
        myJdbcPModel.setMapParams(m);
        return myJdbcTemplate.page(UniqueUserResp.class, myJdbcPModel);
    }
}
