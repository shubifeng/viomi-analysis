package com.viomi.analysis.service.gp.comm;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by asus on 2018/1/26.
 */
@Slf4j
@Service
public class CommonServiceImpl implements CommonService {

    /**
     * Cacheable
     * value：缓存key前缀;
     * key：缓存key后缀;
     * condition: 满足条件才写到缓存;
     * sync：为true时，在多线程环境锁定资源，只允许一个线程进入计算，其他阻塞，避免多次计算，默认是false;
     */
    @Override
//    import org.springframework.cache.annotation.Cacheable;
//    @Cacheable(value = "TestCache", key = "#id", condition = "#id != null", sync = true)
    public Map testCache( Long id ) {
        Map map = new HashMap();
        map.put("Cacheable", id);
        map.put("value：", "缓存key前缀;");
        map.put("key：", "缓存key后缀;");
        map.put("condition: ", "满足条件才写到缓存;");
        map.put("sync：", "不设置默认是false，不阻塞多线程操作;");
        return map;
    }
}
