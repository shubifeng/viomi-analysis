package com.viomi.analysis.service.gp;

import com.alibaba.fastjson.JSONObject;
import com.viomi.base.model.dto.user.CacheUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;


/**
 * Created by shubifeng on 2017/8/29.
 */
@Service
@Slf4j
public class RedisService {

    public static final String TOKEN_PREFIX = "vm__union-auth_";

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 是否存在cacheUser
     *
     * @param token
     * @return
     */
    public Boolean existCacheUser(String token) {
        String key = TOKEN_PREFIX + token;
        return redisTemplate.hasKey(key);
    }

    /**
     * 获取cacheUser
     *
     * @param token
     * @return
     */
    public CacheUser getCacheUser(String token) {
        ValueOperations<String, Object> operations = redisTemplate.opsForValue();
        String key = TOKEN_PREFIX + token;
        String jsonText = operations.get(key).toString();
        CacheUser cacheUser = JSONObject.parseObject(jsonText, CacheUser.class);
        log.debug("{}", cacheUser);
        return JSONObject.parseObject(jsonText, CacheUser.class);
    }

    /*
      保持刷新维持cacheUser
     *
     * @param compId
     * @param userId
     * @return

    public Boolean keepCacheUser(Integer compId, Integer userId) {
        String key = RedisConst.CACHE_USER.redisKey(compId, userId);
        return redisTemplate.expire(key, redisConfig.getMaxAgeInSeconds(), TimeUnit.SECONDS);
    }*/

    /*
      保持刷新维持cacheUser
     *
     * @param compId
     * @param userId
     * @return

    public CacheUser keepCacheUserAndReturn(Integer compId, Integer userId) {
        if (!existCacheUser(compId, userId)) {
            return flushCacheUser(compId, userId);
        }
        String key = RedisConst.CACHE_USER.redisKey(compId, userId);
        Long times = redisTemplate.getExpire(key, TimeUnit.SECONDS);
        if (times < 500) {
            keepCacheUser(compId, userId);
        }
        return getCacheUser(compId, userId);
    }*/

    /**
     * 移除cacheUser
     *
     * @param compId
     * @param userId
     * @return
     */
  /*  public void removeCacheUser(Integer compId, Integer userId) {
        String key = RedisConst.CACHE_USER.redisKey(compId, userId);
        redisTemplate.delete(key);
    }

    public boolean isHasLoginKey(String cookieKey) {
        String redisKey = RedisConst.TOKEN.redisKey(cookieKey);
        return redisTemplate.hasKey(redisKey);
    }*/

    /**
     * 维护登录状态
     *
     * @param cookieKey
     * @return public String keepLoginKey(String cookieKey) {
    String redisKey = RedisConst.TOKEN.redisKey(cookieKey);
    Long times = redisTemplate.getExpire(redisKey, TimeUnit.SECONDS);
    if (times < 120) {
    redisTemplate.expire(redisKey, redisConfig.getMaxAgeInSeconds(), TimeUnit.SECONDS);
    }
    return getLoginKeyValue(cookieKey);
    }*/

    /**
     * 获取用户登录后获取的compId#userId
     *
     * @param cookieKey
     * @return
     */
   /* public String getLoginKeyValue(String cookieKey) {
        String redisKey = RedisConst.TOKEN.redisKey(cookieKey);
        return redisTemplate.opsForValue().get(redisKey).toString();
    }

    *//**
     * 去除登录标记
     *
     * @param cookieKey
     * @return
     *//*
    public void removeLoginKey(String cookieKey) {
        String redisKey = RedisConst.TOKEN.redisKey(cookieKey);
        redisTemplate.delete(redisKey);
    }

    public String get(String key) {
        return (String) redisTemplate.opsForValue().get(key);
    }

    public void set(String key, String val, long seconds) {
        redisTemplate.opsForValue().set(key, val, seconds, TimeUnit.SECONDS);
    }

    public void expire(String key, Integer seconds) {
        redisTemplate.expire(key, seconds, TimeUnit.SECONDS);
    }

    public void delete(String key) {
        redisTemplate.delete(key);
    }

    public Boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    public Long getExpire(String key) {
        return redisTemplate.getExpire(key);
    }*/
}
