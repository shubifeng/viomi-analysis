package com.viomi.analysis.service.gp.fd;

import com.viomi.analysis.comm.constant.SqlXmlPath;
import com.viomi.analysis.comm.constant.report.CommonConstants;
import com.viomi.analysis.comm.constant.report.RefundType;
import com.viomi.analysis.comm.model.dto.resp.OrderRefundPurchaseResp;
import com.viomi.analysis.comm.model.dto.resp.OrderRefundViomiResp;
import com.viomi.analysis.comm.util.ModelUtil;
import com.viomi.analysis.comm.util.export.OrderRefundPurchaseGenerator;
import com.viomi.analysis.comm.util.export.OrderRefundViomiGenerator;
import com.viomi.base.common.model.ParamDate;
import com.viomi.boot.myjdbc.core.MyJdbcPNamedModel;
import com.viomi.boot.myjdbc.core.MyJdbcTemplate;
import com.viomi.boot.myjdbc.page.PageInfo;
import com.viomi.boot.myjdbc.util.SqlMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.List;
import java.util.Map;

/**
 * 发货报表
 *
 * @Author: luocj
 * @Date: Created in 2018/3/27 15:47
 * @Modified:
 */
@Slf4j
@Service
public class OrderRefundServiceImpl implements OrderRefundService {
    private final static Map<String, String> SQL_MAP = SqlMap.loadQueries(SqlXmlPath.ORDER_REFUND);

    @Autowired
    private MyJdbcTemplate gpJdbcTemplate;

    @Override
    public PageInfo<OrderRefundViomiResp> listRefundViomi( Long beginTime, Long endTime, Integer pageNum, Integer pageSize ) {
        MyJdbcPNamedModel myJdbcPNamedModel = ModelUtil.getMyJdbcPNamedModel(SQL_MAP.get(RefundType.VIOMI.getSqlKey()), beginTime, endTime, pageNum, pageSize);
        return gpJdbcTemplate.page(OrderRefundViomiResp.class, myJdbcPNamedModel);
    }

    @Override
    public PageInfo<OrderRefundPurchaseResp> listRefundPurchase( Long beginTime, Long endTime, Integer pageNum, Integer pageSize ) {
        MyJdbcPNamedModel myJdbcPNamedModel = ModelUtil.getMyJdbcPNamedModel(SQL_MAP.get(RefundType.PURCHASE.getSqlKey()), beginTime, endTime, pageNum, pageSize);
        return gpJdbcTemplate.page(OrderRefundPurchaseResp.class, myJdbcPNamedModel);
    }

    @Override
    public Workbook getRefundWorkbook( Integer refundType, ParamDate paramDate, Integer pageNum, Integer pageSize, Integer isExportAll ) {
        StopWatch sw = new StopWatch();

        Workbook wb = null;
        int logCount = 0;
        if (RefundType.VIOMI.getType().equals(refundType)) {
            sw.start("getExportList");
            List<OrderRefundViomiResp> exportList = getExportList(OrderRefundViomiResp.class, SQL_MAP.get(RefundType.VIOMI.getSqlKey()), paramDate, isExportAll, pageNum, pageSize);
            logCount = exportList.size();
            sw.stop();
            //生成Excel
            sw.start("buildWorkbook");
            wb = new OrderRefundViomiGenerator().buildWorkbookForSubSheet(exportList);
            sw.stop();
        } else if (RefundType.PURCHASE.getType().equals(refundType)) {
            sw.start("getExportList");
            List<OrderRefundPurchaseResp> exportList = getExportList(OrderRefundPurchaseResp.class, SQL_MAP.get(RefundType.PURCHASE.getSqlKey()), paramDate, isExportAll, pageNum, pageSize);
            logCount = exportList.size();
            sw.stop();
            //生成Excel
            sw.start("buildWorkbook");
            wb = new OrderRefundPurchaseGenerator().buildWorkbookForSubSheet(exportList);
            sw.stop();
        }
        log.debug("Export data count{}, time[{}-{}], deliverType[{}]", logCount, paramDate.getBegintime(), paramDate.getEndtime(), RefundType.getDesc(refundType));
        log.debug(sw.prettyPrint());

        return wb;
    }

    private <T> List<T> getExportList( Class clazz, String SQL, ParamDate paramDate, Integer isExportAll, Integer pageNum, Integer pageSize ) {
        List<T> exportList;
        if (CommonConstants.YES_OR_NO_YES.equals(isExportAll)) {
            exportList = gpJdbcTemplate.find(clazz, ModelUtil.getMyJdbcNamedModel(SQL, paramDate.getBegintime(), paramDate.getEndtime()));
        } else {
            PageInfo<T> pageInfo = gpJdbcTemplate.page(clazz, ModelUtil.getMyJdbcPNamedModel(SQL, paramDate.getBegintime(), paramDate.getEndtime(), pageNum, pageSize));
            exportList = pageInfo.getList();
        }
        return exportList;
    }

}
