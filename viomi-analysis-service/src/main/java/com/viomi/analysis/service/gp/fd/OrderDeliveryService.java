package com.viomi.analysis.service.gp.fd;

import com.viomi.analysis.comm.model.dto.resp.OrderDeliveryMijiaResp;
import com.viomi.analysis.comm.model.dto.resp.OrderDeliveryPurchaseResp;
import com.viomi.analysis.comm.model.dto.resp.OrderDeliveryViomiResp;
import com.viomi.base.common.model.ParamDate;
import com.viomi.boot.myjdbc.page.PageInfo;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * 发货报表接口类
 *
 * @Author: luocj
 * @Date: Created in 2018/3/27 15:39
 * @Modified:
 */
public interface OrderDeliveryService {

    /**
     * 米家发货报表
     *
     * @param beginTime
     * @param endTime
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageInfo<OrderDeliveryMijiaResp> listDeliveryMijia( Long beginTime, Long endTime, Integer pageNum, Integer pageSize );

    /**
     * 云米商城发货报表
     *
     * @param beginTime
     * @param endTime
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageInfo<OrderDeliveryViomiResp> listDeliveryViomi( Long beginTime, Long endTime, Integer pageNum, Integer pageSize );

    /**
     * 预付款发货报表
     *
     * @param begintime
     * @param endtime
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageInfo<OrderDeliveryPurchaseResp> listDeliveryPurchase( Long begintime, Long endtime, Integer pageNum, Integer pageSize );

    /**
     * 发货报表导出
     *
     * @param deliverType 发货类型 ReportConstants.DELIVERY_TYPE_
     * @param paramDate
     * @param pageNum
     * @param pageSize
     * @param isExportAll 是否导出全部：1-是; 则当前页面
     * @return
     */
    public Workbook getDeliveryWorkbook( Integer deliverType, ParamDate paramDate, Integer pageNum, Integer pageSize, Integer isExportAll );
}
