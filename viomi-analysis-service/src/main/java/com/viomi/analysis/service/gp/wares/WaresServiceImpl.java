package com.viomi.analysis.service.gp.wares;

import com.viomi.analysis.comm.constant.SqlXmlPath;
import com.viomi.analysis.comm.model.dto.resp.WaresSalesDetailResp;
import com.viomi.analysis.comm.model.dto.resp.WaresSalesResp;
import com.viomi.analysis.comm.model.dto.resp.WaresStockResp;
import com.viomi.analysis.repository.gp.ChannelRepository;
import com.viomi.analysis.repository.gp.ProdCatalogRepository;
import com.viomi.base.common.constant.PlatformTypeConst;
import com.viomi.base.common.model.ParamDate;
import com.viomi.boot.myjdbc.core.MyJdbcNamedModel;
import com.viomi.boot.myjdbc.core.MyJdbcTemplate;
import com.viomi.boot.myjdbc.util.SqlMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 商品数据
 *
 * @author
 */
@Slf4j
@Service
public class WaresServiceImpl implements WaresService {

    /**
     * 商品sql
     */
    private final static Map<String, String> SQL_MAP = SqlMap.loadQueries(SqlXmlPath.WARES);

    @Autowired
    private MyJdbcTemplate gpJdbcTemplate;

    @Autowired
    private ProdCatalogRepository prodCatalogRepository;

    @Autowired
    private ChannelRepository channelRepository;

    @Override
    public List<WaresSalesResp> listWaresSales( Long orgId, Long channelId, PlatformTypeConst platformType, ParamDate paramDate ) {
        MyJdbcNamedModel myJdbcNamedModel = new MyJdbcNamedModel();
        Map<String, Object> map = new HashMap<>();
        map.put("v_btimestamp", paramDate.getBegintime());
        map.put("v_etimestamp", paramDate.getEndtime());
        map.put("v_btimestr", paramDate.getBeginDateStr());
        map.put("v_etimestr", paramDate.getEndDateStr());
        map.put("v_platform_type", platformType.name());
        String sql = null;
        //云米和米家
        if (Objects.equals(platformType, PlatformTypeConst.YUN_MI) || Objects.equals(platformType, PlatformTypeConst.MI_JIA)) {
            map.put("v_catalog_id", getUnilifeProductCataLogId());
            sql = String.format(SQL_MAP.get("wares_sales_list"), paramDate.getPgDateFormat(), paramDate.getPgDateFormat(), paramDate.getOrgTimeType().getType());
        } else {
            //天猫和预付款
            map.put("v_channel_id", channelRepository.getSubChannelIdsByRecursive(channelId, null, orgId, null));
            sql = String.format(SQL_MAP.get("wares_sales_list_by_channel"), paramDate.getPgDateFormat(), paramDate.getPgDateFormat(), paramDate.getOrgTimeType().getType());
        }
        myJdbcNamedModel.setSql(sql);
        myJdbcNamedModel.setMapParams(map);
        return gpJdbcTemplate.find(WaresSalesResp.class, myJdbcNamedModel);
    }

    @Override
    public List<WaresSalesDetailResp> listWaresSalesDetail( Long orgId, Long channelId, PlatformTypeConst platformType, ParamDate paramDate ) {
        Map<String, Object> map = new HashMap<>();
        map.put("v_btimestamp", paramDate.getBegintime());
        map.put("v_etimestamp", paramDate.getEndtime());
        map.put("v_platform_type", platformType.name());

        MyJdbcNamedModel myJdbcNamedModel = new MyJdbcNamedModel();
        myJdbcNamedModel.setMapParams(map);

        //云米和米家
        if (Objects.equals(platformType, PlatformTypeConst.YUN_MI) || Objects.equals(platformType, PlatformTypeConst.MI_JIA)) {
            map.put("v_catalog_id", getUnilifeProductCataLogId());
            myJdbcNamedModel.setSql(SQL_MAP.get("wares_sales_detail"));
        } else {
            //天猫和预付款
            map.put("v_channel_id", channelRepository.getSubChannelIdsByRecursive(channelId, null, orgId, null));
            myJdbcNamedModel.setSql(SQL_MAP.get("wares_sales_detail_by_channel"));
        }
        return gpJdbcTemplate.find(WaresSalesDetailResp.class, myJdbcNamedModel);
    }

    @Override
    public List<WaresStockResp> listWaresStock( String prodName, Integer saleStatus, String catalogIds, String stockCountRange, String pendingCountRange ) {
        Map<String, Object> map = new HashMap<>();
        //销售状态：0预售，1在售
        List<Integer> saleStatusList = saleStatus == null ? Arrays.asList(0, 1) : Arrays.asList(saleStatus);
        map.put("v_prod_status", saleStatusList);
        //商品搜索
        if (StringUtils.isNotBlank(prodName)) {
            map.put("v_prod_name", "%" + StringUtils.trim(prodName) + "%");
        } else {
            map.put("v_prod_name", null);
        }

        //商品库存范围值
        if (StringUtils.isNotBlank(stockCountRange)) {
            String[] countRangeArr = StringUtils.split(stockCountRange, ",");
            map.put("v_limit_min_stock", Integer.parseInt(countRangeArr[0]));
            map.put("v_limit_max_stock", Integer.parseInt(countRangeArr[1]));
        } else {
            map.put("v_limit_min_stock", 0);
            map.put("v_limit_max_stock", 0);
        }

        //已售商品待发货范围值
        if (StringUtils.isNotBlank(pendingCountRange)) {
            String[] countRangeArr = StringUtils.split(pendingCountRange, ",");
            map.put("v_limit_min_pending_count", Integer.parseInt(countRangeArr[0]));
            map.put("v_limit_max_pending_count", Integer.parseInt(countRangeArr[1]));
        } else {
            map.put("v_limit_min_pending_count", 0);
            map.put("v_limit_max_pending_count", 0);
        }

        String sql = SQL_MAP.get("wares_prod_list");
        //商品类目-使用%s替换
        if (StringUtils.isNotBlank(catalogIds)) {
            String[] arr = StringUtils.split(catalogIds, ',');
            long[] idArr = Arrays.stream(arr).mapToLong(Long::valueOf).toArray();
            String catalogId = " and catalog_id in (select id from vm_wares.prod_catalog ct where ct.parent_id in (" + StringUtils.join(idArr, ',') + "))";
            sql = String.format(sql, catalogId);
        } else {
            sql = String.format(sql, "");
        }
        MyJdbcNamedModel myJdbcNamedModel = new MyJdbcNamedModel();
        myJdbcNamedModel.setMapParams(map);
        myJdbcNamedModel.setSql(sql);
        return gpJdbcTemplate.find(WaresStockResp.class, myJdbcNamedModel);
    }

    /**
     * 查询优悦商品
     *
     * @return
     */
    private Iterable<Long> getUnilifeProductCataLogId() {
        //优悦商品类型code
        String code = "UNILIFE_PRODUCT";
        return prodCatalogRepository.getIds(code);
    }

}
