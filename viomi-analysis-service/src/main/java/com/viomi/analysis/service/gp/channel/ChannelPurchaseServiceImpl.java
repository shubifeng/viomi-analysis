package com.viomi.analysis.service.gp.channel;

import com.viomi.analysis.comm.constant.SqlXmlPath;
import com.viomi.analysis.comm.model.dto.resp.ChannelPurchaseOrderDetailResp;
import com.viomi.analysis.comm.model.dto.resp.ChannelPurchaseOrderResp;
import com.viomi.analysis.repository.gp.ChannelRepository;
import com.viomi.base.common.model.ParamDate;
import com.viomi.boot.myjdbc.core.MyJdbcNamedModel;
import com.viomi.boot.myjdbc.core.MyJdbcPNamedModel;
import com.viomi.boot.myjdbc.core.MyJdbcTemplate;
import com.viomi.boot.myjdbc.page.PageInfo;
import com.viomi.boot.myjdbc.util.SqlMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 预付款接口服务
 *
 * @Author: luocj
 * @Date: Created in 2018/2/5 14:17
 * @Modified:
 */
@Service
@Slf4j
public class ChannelPurchaseServiceImpl implements ChannelPurchaseService {
    private final static Map<String, String> SQL_MAP = SqlMap.loadQueries(SqlXmlPath.CHANNEL_PURCHASE);

    @Autowired
    private MyJdbcTemplate gpJdbcTemplate;
    @Autowired
    private ChannelRepository channelRepository;

    @Override
    public List<ChannelPurchaseOrderResp> listOrderForChart(Integer orderType, ParamDate paramDate) {
        Map<String, Object> map = new HashMap<>();
        map.put("v_btimestamp", paramDate.getBegintime());
        map.put("v_etimestamp", paramDate.getEndtime());
        map.put("v_btimestr", paramDate.getBeginDateStr());
        map.put("v_etimestr", paramDate.getEndDateStr());
        map.put("v_orderType", orderType);

        MyJdbcNamedModel myJdbcNamedModel = new MyJdbcNamedModel();
        myJdbcNamedModel.setMapParams(map);
        String sql = String.format(SQL_MAP.get("channel_purchase_order_chart"), paramDate.getPgDateFormat(), paramDate.getPgDateFormat(), paramDate.getOrgTimeType().getType());
        myJdbcNamedModel.setSql(sql);
        return gpJdbcTemplate.find(ChannelPurchaseOrderResp.class, myJdbcNamedModel);
    }

    @Override
    public PageInfo<ChannelPurchaseOrderDetailResp> listOrderForChartDetail(Integer orderType, Long beginTime, Long endTime, Integer pageNum, Integer pageSize) {
        Map<String, Object> map = new HashMap<>();
        map.put("v_btimestamp", beginTime);
        map.put("v_etimestamp", endTime);
        map.put("v_orderType", orderType);

        MyJdbcPNamedModel myJdbcPNamedModel = new MyJdbcPNamedModel();
        myJdbcPNamedModel.setMapParams(map);
        String sql = String.format(SQL_MAP.get("channel_purchase_order_chart_detail"), beginTime, endTime);
        myJdbcPNamedModel.setSql(sql);

        //分页信息
        myJdbcPNamedModel.setPageNo(pageNum);
        myJdbcPNamedModel.setPageSize(pageSize);

        return gpJdbcTemplate.page(ChannelPurchaseOrderDetailResp.class, myJdbcPNamedModel);
    }

}
