package com.viomi.analysis.service.gp.user;

import com.viomi.analysis.comm.model.dto.req.UserSearchReq;
import com.viomi.analysis.comm.model.dto.resp.UniqueUserResp;
import com.viomi.boot.myjdbc.page.PageInfo;

/**
 * Created by asus on 2018/3/29.
 */
public interface UserService {

    PageInfo<UniqueUserResp> list(UserSearchReq userSearchReq);

    PageInfo<UniqueUserResp> listShenJiUser(int pageNum, int pageSize, String sourceType, String userName, String phone, String nickname, String queryTime);
}
