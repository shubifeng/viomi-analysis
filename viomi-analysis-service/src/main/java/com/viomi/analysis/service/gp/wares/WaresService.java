package com.viomi.analysis.service.gp.wares;

import com.viomi.analysis.comm.model.dto.resp.WaresSalesDetailResp;
import com.viomi.analysis.comm.model.dto.resp.WaresSalesResp;
import com.viomi.analysis.comm.model.dto.resp.WaresStockResp;
import com.viomi.base.common.constant.PlatformTypeConst;
import com.viomi.base.common.model.ParamDate;

import java.util.List;

/**
 * 商品销售
 *
 * @author Shubifeng
 */
public interface WaresService {

    /**
     * 商品销售（以时间为维度汇总销量/销售额）
     * @param orgId
     * @param channelId
     * @param platformType
     * @param paramDate
     * @return
     */
    List<WaresSalesResp> listWaresSales(Long orgId, Long channelId, PlatformTypeConst platformType, ParamDate paramDate);

    /**
     * 商品销售（以商品为维度汇总商品销量/销售额明细）
     * @param orgId
     * @param channelId
     * @param platformType
     * @param paramDate
     * @return
     */
    List<WaresSalesDetailResp> listWaresSalesDetail(Long orgId, Long channelId, PlatformTypeConst platformType, ParamDate paramDate);

    /**
     * 商品库列表
     * @param prodName
     * @param saleStatus
     * @param catalogIds
     * @param stockCountRange
     * @param pendingCountRange
     * @return
     */
    List<WaresStockResp> listWaresStock(String prodName, Integer saleStatus, String catalogIds, String stockCountRange, String pendingCountRange);
}
