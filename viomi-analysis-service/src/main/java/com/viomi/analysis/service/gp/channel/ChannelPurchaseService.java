package com.viomi.analysis.service.gp.channel;

import com.viomi.analysis.comm.model.dto.resp.ChannelPurchaseOrderDetailResp;
import com.viomi.analysis.comm.model.dto.resp.ChannelPurchaseOrderResp;
import com.viomi.base.common.model.ParamDate;
import com.viomi.boot.myjdbc.page.PageInfo;

import java.util.List;

/**
 * 渠道预付款信息查询
 *
 * @Author: luocj
 * @Date: Created in 2018/1/29 13:49
 * @Modified:
 */
public interface ChannelPurchaseService {

    /**
     * 1.1-预付款提货-图表
     * @param orderType
     * @param paramDate
     * @return
     */
    List<ChannelPurchaseOrderResp> listOrderForChart(Integer orderType, ParamDate paramDate);

    /**
     * 1.2-预付款提货-报表明细
     * @param orderType
     * @param beginTime
     * @param endTime
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo<ChannelPurchaseOrderDetailResp> listOrderForChartDetail(Integer orderType, Long beginTime, Long endTime, Integer pageNum, Integer pageSize);

}
